﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Helpers
{
    public static class CIDExtensionsHelper
    {
        /// <summary>
        /// Convierte una lista a datatable, pero solamente las propiedades basicas (string, int, decimal, etc),
        /// no objetos de otras clases (UserModel User, VentaModel Venta)
        /// </summary>
        /// <typeparam name="T">Lista</typeparam>
        /// <param name="iList">Lista a convertir a Datatable</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this List<T> iList)
        {
            List<string> columnasNoValidas = new List<string>();

            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);

                dataTable.Columns.Add(propertyDescriptor.Name, type);

                if (!type.IsPrimitiveType())
                {
                    columnasNoValidas.Add(propertyDescriptor.Name);
                }
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }

            foreach (string columnaNoValida in columnasNoValidas)
            {
                dataTable.Columns.Remove(columnaNoValida);
            }

            return dataTable;
        }

        public static bool IsPrimitiveType(this Type type)
        {
            return
             type == typeof(Object) ||
             type == typeof(String) ||
             type == typeof(Char) ||
             type == typeof(Boolean) ||
             type == typeof(Byte) ||
             type == typeof(Int16) ||
             type == typeof(Int32) ||
             type == typeof(Int64) ||
             type == typeof(UInt16) ||
             type == typeof(UInt32) ||
             type == typeof(UInt64) ||
             type == typeof(IntPtr) ||
             type == typeof(Single) ||
             type == typeof(Double) ||
             type == typeof(Guid) ||
             type == typeof(Decimal);
        }
    }
}
