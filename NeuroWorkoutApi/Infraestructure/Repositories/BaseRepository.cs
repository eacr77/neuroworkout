﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories
{
    public class BaseRepository
    {
        private string ConnectionString { get; }

        protected IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConnectionString);
            }
        }

        public BaseRepository(IConfiguration config)
        {
            ConnectionString = config.GetConnectionString("ConnectionDB");
        }
    }
}
