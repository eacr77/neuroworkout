﻿using NeuroWorkoutApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories.TrainingRepo
{
    public interface ITrainingRepo
    {
        Task<EjercicioModel> GetEjercicioXId(Guid IdEjercicio);
        Task<List<EjercicioWodModel>> GetEjerciciosWod(Guid IdWod,int TipoWod);
        Task<List<EjercicioModel>> GetAllEjercicios(int opc);
        Task<int> SetResultadoWod(WodResultModel model);
        Task<WodCompleto> GetWodDia(DateTime fecha);
        Task<WodCompleto> GetWodTest(int idTipoWod);
    }
}
