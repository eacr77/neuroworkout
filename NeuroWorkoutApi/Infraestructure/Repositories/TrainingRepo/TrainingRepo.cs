﻿using Dapper;
using Microsoft.Extensions.Configuration;
using NeuroWorkoutApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories.TrainingRepo
{
    public class TrainingRepo : ITrainingRepo
    {
        private readonly IConfiguration _config;
        public TrainingRepo(IConfiguration config)
        {
            _config = config;
        }
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("ConnectionDB"));
            }
        }
        public async Task<List<EjercicioModel>> GetAllEjercicios(int opcion)
        {
            try
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Opcion", opcion);
                List<EjercicioModel> obj = new List<EjercicioModel>();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryAsync<EjercicioModel>(
                    "[dbo].[SP_Get_ListaEjercicios]",
                    param: dynamicParameters,
                     commandType: CommandType.StoredProcedure);
                    obj = result.ToList();
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<List<EjercicioWodModel>> GetEjerciciosWod(Guid IdWod,int TipoWod)
        {
            try
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdWod", IdWod);
                dynamicParameters.Add("@TipoWod", TipoWod);
                List<EjercicioWodModel> obj = new List<EjercicioWodModel>();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryAsync<EjercicioWodModel>(
                    "[dbo].[SP_Get_EjerciciosWod]",
                    param: dynamicParameters,
                     commandType: CommandType.StoredProcedure);
                    obj = result.ToList();
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<EjercicioModel> GetEjercicioXId(Guid IdEjercicio)
        {
            try
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdEjercicio", IdEjercicio);
                EjercicioModel obj = new EjercicioModel();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryFirstOrDefaultAsync<EjercicioModel>(
                    "[dbo].[SP_Get_EjercicioXId]",
                    param: dynamicParameters,
                     commandType: CommandType.StoredProcedure);
                    obj = result;
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<WodCompleto> GetWodDia(DateTime fecha)
        {
            try
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Fecha", fecha);
                WodCompleto obj = new WodCompleto();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryFirstOrDefaultAsync<WodCompleto>(
                    "[dbo].[SP_Get_WodDia]",
                    param: dynamicParameters,
                     commandType: CommandType.StoredProcedure);
                    obj = result;
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<WodCompleto> GetWodTest(int idTipoWod)
        {
            try
            {
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdTipoWod", idTipoWod);
                WodCompleto obj = new WodCompleto();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryFirstOrDefaultAsync<WodCompleto>(
                    "[dbo].[SP_Get_WodTest]",
                    param: dynamicParameters,
                     commandType: CommandType.StoredProcedure);
                    obj = result;
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<int> SetResultadoWod(WodResultModel model)
        {
            try
            {
                int result;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdWod", model.IdWod);
                dynamicParameters.Add("@IdCliente", model.IdCliente);
                dynamicParameters.Add("@Calificacion", model.Calificacion);
                dynamicParameters.Add("@Restrospectiva", model.Restrospectiva);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<int>(
                   "[dbo].[SP_Set_ResultadoWod]",
                    param: dynamicParameters,
                   commandType: CommandType.StoredProcedure);

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
