﻿using Dapper;
using Microsoft.Extensions.Configuration;
using NeuroWorkoutApi.Infraestructure.Helpers;
using NeuroWorkoutApi.Models;
using NeuroWorkoutApi.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories.TestsRepo
{
    public class TestRepo : ITestRepo
    {
        private readonly IConfiguration _config;
        public TestRepo(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("ConnectionDB"));
            }
        }
        public async Task<List<PreguntaModel>> GetEncuesta(int opcion)
        {
            try
            {
                List<PreguntaModel> test = new List<PreguntaModel>();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Opc", opcion);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryAsync<PreguntaModel>(
                    "[dbo].[SP_Get_Encuesta]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);
                    test = result.ToList();
                }
                return test;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<int> SetEncuesta(TestModel model)
        {
            try
            {
                int result;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdTest", model.IdTest);
                dynamicParameters.Add("@IdCliente", model.IdCliente);
                dynamicParameters.Add("@PuntajeEstres", model.PuntajeEstres);
                dynamicParameters.Add("@PuntajeAnsiedad", model.PuntajeAnsiedad);
                dynamicParameters.Add("@PuntajeDepresion", model.PuntajeDepresion);
                dynamicParameters.Add("@ResultadoEstres", model.ResultadoEstres);
                dynamicParameters.Add("@ResultadoAnsiedad", model.ResultadoAnsiedad);
                dynamicParameters.Add("@ResultadoDepresion", model.ResultadoDepresion);
                dynamicParameters.Add("@EsInicial", model.EsInicial);
                dynamicParameters.Add("@Tabla", model.Respuestas.ToDataTable()
                        .AsTableValuedParameter("[dbo].[RespuestasPreguntas]"));
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<int>(
                    "[dbo].[SP_Set_EncuestaCliente]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
