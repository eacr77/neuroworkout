﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NeuroWorkoutApi.Models;
using NeuroWorkoutApi.Models.Dto;
namespace NeuroWorkoutApi.Infraestructure.Repositories.TestsRepo
{
  public  interface ITestRepo
    {
        Task<List<PreguntaModel>> GetEncuesta(int opcion);
        Task<int> SetEncuesta(TestModel model);
    }
}
