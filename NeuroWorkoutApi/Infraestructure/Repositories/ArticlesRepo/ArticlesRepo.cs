﻿using Dapper;
using Microsoft.Extensions.Configuration;
using NeuroWorkoutApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories.ArticlesRepo
{
    public class ArticlesRepo:IArticlesRepo
    {
        private readonly IConfiguration _config;
        public ArticlesRepo(IConfiguration config)
        {
            _config = config;
        }
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("ConnectionDB"));
            }
        }
        public async Task<List<ArticleModel>> GetAllArticles()
        {
            try
            {
                List<ArticleModel> test = new List<ArticleModel>();
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryAsync<ArticleModel>(
                    "[dbo].[SP_Get_Articulos]",
                    commandType: CommandType.StoredProcedure);
                    test = result.ToList();
                }
                return test;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
