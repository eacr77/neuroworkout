﻿using NeuroWorkoutApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Repositories.ArticlesRepo
{
    public interface IArticlesRepo
    {
        Task<List<ArticleModel>> GetAllArticles();
    }
}
