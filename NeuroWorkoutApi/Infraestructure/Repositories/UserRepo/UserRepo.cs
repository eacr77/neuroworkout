﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using NeuroWorkoutApi.Models;
using Dapper;
using NeuroWorkoutApi.Services.Email;
using Microsoft.Extensions.Options;

namespace NeuroWorkoutApi.Infraestructure.Repositories.UserRepo
{
    public class UserRepo : IUserRepo
    {
        private readonly IConfiguration _config;
        private readonly IEmailService _emailService;
        private readonly EmailModel _mailSettings;
        public UserRepo(IConfiguration config, IEmailService emailService, IOptions<EmailModel> mailSettings)
        {
            _config = config;
            _emailService = emailService;
            _mailSettings = mailSettings.Value;
        }
        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("ConnectionDB"));
            }
        }
        public async Task<User> Login(User user)
        {
            try
            {
                User obj = new User();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Usuario", user.Usuario);
                dynamicParameters.Add("@Password", user.Contraseña);
                dynamicParameters.Add("@TipoUsuario", user.IdTipoUsuario);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryFirstOrDefaultAsync<User>(
                    "[dbo].[SP_Login]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);
                    obj = result;
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<User> GetUser(Guid IdUser)
        {
            try
            {
                User obj = new User();
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdCliente", IdUser);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    var result = await conn.QueryFirstOrDefaultAsync<User>(
                    "[dbo].[SP_Get_ClienteXId]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);
                    obj = result;
                }
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<int> NewUser(User user)
        {
            try
            {
                int result;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Nombre", user.NombreCompleto);
                dynamicParameters.Add("@FechaNacimiento", user.FechaNacimiento);
                dynamicParameters.Add("@Peso", user.Peso);
                dynamicParameters.Add("@Altura", user.Altura);
                dynamicParameters.Add("@Imc", user.Imc);
                dynamicParameters.Add("@Telefono", user.Telefono);
                dynamicParameters.Add("@Correo", user.Correo);
                dynamicParameters.Add("@Sexo", user.Sexo);
                dynamicParameters.Add("@Usuario", user.Usuario);
                dynamicParameters.Add("@Password", user.Contraseña);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<int>(
                    "[dbo].[SP_A_Cliente]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<int> UpdateUser(User user)
        {
            try
            {
                int result;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdCliente", user.IdUsuario);
                dynamicParameters.Add("@Nombre", user.NombreCompleto);
                dynamicParameters.Add("@FechaNacimiento", user.FechaNacimiento);
                dynamicParameters.Add("@Peso", user.Peso);
                dynamicParameters.Add("@Altura", user.Altura);
                dynamicParameters.Add("@Imc", user.Imc);
                dynamicParameters.Add("@Telefono", user.Telefono);
                dynamicParameters.Add("@Correo", user.Correo);
                dynamicParameters.Add("@Sexo", user.Sexo);
               // dynamicParameters.Add("@Foto", user.Foto);
                dynamicParameters.Add("@Usuario", user.Usuario);
                dynamicParameters.Add("@Password", user.Contraseña);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<int>(
                    "[dbo].[SP_C_Cliente]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<int> DeleteUser(Guid IdCliente)
        {
            try
            {
                int result;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@IdCliente", IdCliente);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<int>(
                    "[dbo].[SP_B_Cliente]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<Guid?> RecoveryUser(string Email)
        {
            try
            {
                string result;
                Guid g;
                var dynamicParameters = new DynamicParameters();
                dynamicParameters.Add("@Email", Email);
                using (IDbConnection conn = Connection)
                {
                    conn.Open();
                    result = await conn.QueryFirstOrDefaultAsync<string>(
                    "[dbo].[SP_Get_NvaContraseña]",
                     param: dynamicParameters,
                    commandType: CommandType.StoredProcedure);

                }
                if (result != null)
                { 
                    SendNewPasswordByEmail(result.ToString(), _mailSettings, Email);
                   return g = new Guid(result);
                }
                else
                {
                    return null;
                }
                
              
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private string SendNewPasswordByEmail(string newPassword, EmailModel emailModel,string UserMail)
        {
            try
            {
                string subject = "NeuroWorkout";
                string body = "<!DOCTYPE html><html><div style='width: 100%; height: 200px; background-color: #fff;'>" +
                    "<div class='greenSeparator' style='background-color: #174188; width: 100%; height: 30px; margin-bottom: 10px;'></div>" +
                    "<div class='logo' style='background-image: url(http://naturalwuane.com/img/logo.png);background-size: contain;" +
                    "background-repeat: no-repeat;" +
                    "background-position: center;" +
                    "width: 250px; height: 150px; display: block; margin: auto;'>" +
                    "</div>" +
                    "</div>" +
                    "<div style ='color: #333; background-color: #fff; display: block; margin-top: 0px; font-family: sans-serif;'>" +
                    "<h3 style='text-align: center; font-size: 26px; padding: 10px 0px 3px 0px; margin-bottom: 0'>Recuperacion de contraseña</h3>" +
                    "<p style = 'text-align: center; padding: 10px 0px;' > Buen día, lamentamos que haya perdido su contraseña, para acceder nuevamente a la app de NeuroWorkout le enviamos su nuevo password</p>" +
                    "<p style = 'text-align: center; padding: 10px 0px;' > Su nuevo password es: <strong>" + newPassword + "</strong> le aconsejamos que una vez que ingrese a la App, cambie este password en su perfil</p>" +
                    "</table>" +
                    "</div>" +
                    "<div style = 'background-color: #174188; height: 20px; width: 100%; padding: 11px 0px;' >" +
                    "<p style='margin: 0; color: #fff; text-align: center; font-family: sans-serif; font-size: 11px;'>*Cualquier error con el correo favor de contactar con soporte técnico.</p>" +
                    "<p style='margin: 0; color: #fff; text-align: center; font-family: sans-serif; font-size: 11px;'> Atentamente el equipo de<strong> NeuroWorkout</strong></p>" +
                    "</div></html>";

                _emailService.SetConfiguration(emailModel.SenderMail, emailModel.Password, emailModel.Host, emailModel.Port, emailModel.EnableSsl);
                _emailService.SendEmail(subject, body, new List<string> { UserMail }, true);

                return "Verifique su email, se le ha enviado una nueva contraseña.";
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
            
        }
    }
}
