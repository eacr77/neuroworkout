﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NeuroWorkoutApi.Models;

namespace NeuroWorkoutApi.Infraestructure.Repositories.UserRepo
{
   public interface IUserRepo
    {
        Task<User> Login(User user);
        Task<User> GetUser(Guid IdUser);
        Task<int> NewUser(User user);
        Task<int> UpdateUser(User user);
        Task<int> DeleteUser(Guid IdCliente);
        Task<Guid?> RecoveryUser(string Email);
    }
}
