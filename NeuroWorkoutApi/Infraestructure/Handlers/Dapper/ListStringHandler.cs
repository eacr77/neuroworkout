﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Infraestructure.Handlers.Dapper
{
    public class ListStringHandler : SqlMapper.TypeHandler<List<string>>
    {
        /// <summary>
        /// Cuando la base de datos regresa datos
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override List<string> Parse(object value)
        {
            return new List<string>
            {
                value.ToString()
            };
        }

        /// <summary>
        /// Cuando envias a la base de datos
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="value"></param>
        public override void SetValue(IDbDataParameter parameter, List<string> value)
        {
            throw new NotImplementedException();
        }
    }
}
