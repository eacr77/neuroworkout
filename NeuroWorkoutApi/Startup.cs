using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NeuroWorkoutApi.Infraestructure.Filters;
using NeuroWorkoutApi.Infraestructure.Handlers.Dapper;
using NeuroWorkoutApi.Infraestructure.Repositories.ArticlesRepo;
using NeuroWorkoutApi.Infraestructure.Repositories.TestsRepo;
using NeuroWorkoutApi.Infraestructure.Repositories.TrainingRepo;
using NeuroWorkoutApi.Infraestructure.Repositories.UserRepo;
using NeuroWorkoutApi.Models;
using NeuroWorkoutApi.Services.Email;

namespace NeuroWorkoutApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                options.Filters.Add(typeof(ValidateModelStateFilter));

            });
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IUserRepo, UserRepo>();
            services.AddTransient<ITrainingRepo, TrainingRepo>();
            services.AddTransient<ITestRepo, TestRepo>();
            services.AddTransient<IArticlesRepo, ArticlesRepo>();
            services.Configure<EmailModel>(Configuration.GetSection("StmpConfig"));
            SqlMapper.AddTypeHandler(new ListStringHandler());
            //services.AddControllers();
            
           //return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
