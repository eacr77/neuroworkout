﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Services.Email
{
    public class EmailService : IEmailService
    {
        private SmtpClient _smtpClient;
        protected string SenderMail { get; set; }

        public void SetConfiguration(string senderMail, string password, string host, int port, bool enableSsl)
        {
            SenderMail = senderMail;

            _smtpClient = new SmtpClient();
            _smtpClient.Credentials = new NetworkCredential(senderMail, password);
            _smtpClient.Host = host;
            _smtpClient.Port = port;
            _smtpClient.EnableSsl = enableSsl;
        }

        public void SendEmail(string subject, string body, List<string> recipientMail, bool IsbodyHtml)
        {
            if (_smtpClient is null)
            {
                throw new Exception("Init your instance");
            }

            var mailMessage = new MailMessage();
            try
            {
                mailMessage.From = new MailAddress(SenderMail);
                foreach (string mail in recipientMail)
                {
                    mailMessage.To.Add(mail);
                }
                mailMessage.IsBodyHtml = IsbodyHtml;
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.Priority = MailPriority.Normal;
                _smtpClient.Send(mailMessage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mailMessage.Dispose();
                _smtpClient.Dispose();
            }
        }
    }
}
