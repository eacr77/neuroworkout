﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Services.Email
{
   public interface IEmailService
    {
        void SetConfiguration(string senderMail, string password, string host, int port, bool enableSsl);
        void SendEmail(string subject, string body, List<string> recipientMail, bool IsbodyHtml);
    }
}
