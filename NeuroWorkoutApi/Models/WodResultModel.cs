﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class WodResultModel
    {
        public Guid IdWod { get; set; }
        public Guid IdCliente { get; set; }
        public int Calificacion { get; set; }
        public string Restrospectiva { get; set; }
    }
}
