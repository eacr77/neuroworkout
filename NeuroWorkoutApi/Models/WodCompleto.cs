﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class WodCompleto
    {
        public Guid Id { get; set; }
        public string NombreWod { get; set; }
        public string Nivel { get; set; }
        public string TipoEstimulacion { get; set; }
        public Guid IdEstiramiento { get; set; }
        public Guid IdCalentamiento { get; set; }
        public int TiempoTotalSegundos { get; set; }
        public int TiempoTotalMinutos { get; set; }
        public int TotalRound { get; set; }
        public int TiempoLimiteSegundos { get; set; }
        public Guid IdEstiramiento2 { get; set; }
    }
}
