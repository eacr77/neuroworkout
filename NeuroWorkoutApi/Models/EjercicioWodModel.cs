﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class EjercicioWodModel
    {
        public Guid IdEjercicio { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }
        public string UrlMedia { get; set; }
        public int Orden { get; set; }
        public bool BandCantidad { get; set; }
        public bool BandTiempo { get; set; }
        public bool BandDistancia { get; set; }
        public int Cantidad { get; set; }
        public int Tiempo { get; set; }
        public int Metros { get; set; }
        public bool EsVideo { get; set; }
    }
}
