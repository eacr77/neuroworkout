﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class RespuestaModel
    {
      
        public Guid IdPreguntaTest { get; set; }
       
        public int Respuesta { get; set; }
    }
}
