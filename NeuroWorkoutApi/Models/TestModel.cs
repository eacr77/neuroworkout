﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class TestModel
    {
        public bool EsInicial { get; set; }
        public Guid IdTest { get; set; }
        public Guid IdCliente { get; set; }
        public int NivelEstres { get; set; }
        public int NivelAnsiedad { get; set; }
        public int NivelDepresion { get; set; }
        public int PuntajeEstres { get; set; }
        public int PuntajeAnsiedad { get; set; }
        public int PuntajeDepresion { get; set; }
        public string ResultadoEstres { get; set; }
        public string ResultadoDepresion { get; set; }
        public string ResultadoAnsiedad { get; set; }
        public List<RespuestaModel> Respuestas { get; set; }
    }
}
