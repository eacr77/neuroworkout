﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class ArticleModel
    {
        public string Nombre { get; set; }
        public string TipoA { get; set; }
        public string Fecha { get; set; }
        public string UrlArchivo { get; set; }
        public string Foto { get; set; }
    }
}
