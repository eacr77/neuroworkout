﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models.Dto
{
    public class RespuestaEncuestaModel
    {
        public Guid IdTest { get; set; }
        public Guid IdCliente { get; set; }
        public int PuntajeEstres { get; set; }
        public int PuntajeAnsiedad { get; set; }
        public int PuntajeDepresion { get; set; }
        public string ResultadoEstres { get; set; }
        public string ResultadoDepresion { get; set; }
        public string ResultadoAnsiedad { get; set; }
        public List<RespuestaModel> Respuestas { get; set; }
    }
}
