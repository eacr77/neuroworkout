﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Models
{
    public class PreguntaModel
    {
        public Guid IdTest { get; set; }
        public Guid IdPreguntaTest { get; set; }        
        public string Pregunta { get; set; }
        public int Orden { get; set; }
        public int Valor { get; set; }
    }
}
