﻿using Microsoft.AspNetCore.Mvc;
using NeuroWorkoutApi.Infraestructure.Repositories.ArticlesRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ArticleController : ControllerBase
    {
        private readonly IArticlesRepo repository;
        public ArticleController(IArticlesRepo repo)
        {
            this.repository = repo;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllArticles()
        {
            var result = await repository.GetAllArticles();
            return Ok(result);
        }
    }
}
