﻿using Microsoft.AspNetCore.Mvc;
using NeuroWorkoutApi.Infraestructure.Repositories.TrainingRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NeuroWorkoutApi.Models;
namespace NeuroWorkoutApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TrainingController : ControllerBase
    {
        private readonly ITrainingRepo repository;
        public TrainingController(ITrainingRepo repo)
        {
            this.repository = repo;
        }
        [HttpPost]
        public async Task<IActionResult> GetEjercicioXId([FromBody] Guid IdEjercicio)
        {
            var result = await repository.GetEjercicioXId(IdEjercicio);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> GetEjerciciosWod([FromBody]object[] valores)
        {
            try
            {
                string str = valores[0].ToString();
                var idWod = new Guid(str);
                int tipo = Convert.ToInt32(valores[1].ToString());
                var result = await repository.GetEjerciciosWod(idWod, tipo);
                return Ok(result);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        [HttpPost]
        public async Task<IActionResult> GetAllEjercicios([FromBody] int opc)
        {
            var result = await repository.GetAllEjercicios(opc);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> SetResultadoWod([FromBody] WodResultModel model)
        {
            var result = await repository.SetResultadoWod(model);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> GetWodDia([FromBody] DateTime fecha)
        {
            var result = await repository.GetWodDia(fecha);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> GetWodTest([FromBody] int idTipoWod)
        {
            var result = await repository.GetWodTest(idTipoWod);
            return Ok(result);
        }
    }
}
