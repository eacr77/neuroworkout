﻿using Microsoft.AspNetCore.Mvc;
using NeuroWorkoutApi.Infraestructure.Repositories.TestsRepo;
using NeuroWorkoutApi.Models;
using NeuroWorkoutApi.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class TestController : ControllerBase
    {
        private readonly ITestRepo repository;
        public TestController(ITestRepo repo)
        {
            this.repository = repo;
        }
        [HttpPost]
        public async Task<IActionResult> GetEncuesta([FromBody] int opc)
        {
            var result = await repository.GetEncuesta(opc);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> SetEncuesta([FromBody] TestModel model)
        {
            var result = await repository.SetEncuesta(model);
            return Ok(result);
        }
    }
}
