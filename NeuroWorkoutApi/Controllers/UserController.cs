﻿using Microsoft.AspNetCore.Mvc;
using NeuroWorkoutApi.Infraestructure.Repositories.UserRepo;
using NeuroWorkoutApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuroWorkoutApi.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    
    public class UserController : ControllerBase
    {
        private readonly IUserRepo repository;
        public UserController(IUserRepo repo)
        {
            this.repository = repo;
        }
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] User user)
        {
            var result = await repository.Login(user);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> GetUser([FromBody] Guid IdUsuario)
        {
            var result = await repository.GetUser(IdUsuario);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> NewUser([FromBody] User user)
        {
            var result = await repository.NewUser(user);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateUser([FromBody] User user)
        {
            var result = await repository.UpdateUser(user);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> DeleteUser([FromBody] Guid IdUser)
        {
            var result = await repository.DeleteUser(IdUser);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> RecoveryUser([FromBody] string Email)
        {
            var result = await repository.RecoveryUser(Email);
            return Ok(result);
        }
    }
}
