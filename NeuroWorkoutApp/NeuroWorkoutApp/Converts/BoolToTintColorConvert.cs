﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Converts
{
   public class BoolToTintColorConvert : IValueConverter
    {
        public Color UnSelectedColor { get; set; }
        public Color SelectedColor { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is bool band)
                {
                    return (band ? SelectedColor : UnSelectedColor);
                }
            }
            return UnSelectedColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
