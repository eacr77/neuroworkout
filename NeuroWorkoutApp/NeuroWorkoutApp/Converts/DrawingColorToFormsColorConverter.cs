﻿using System;
using System.Globalization;
using Xamarin.Forms;
namespace NeuroWorkoutApp.Converts
{
    public class DrawingColorToFormsColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is System.Drawing.Color color)
            {
                Color newColor = new Color(color.R, color.G, color.B, color.A);
                return newColor;
            }
            return Color.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
