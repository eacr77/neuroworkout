﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Converts
{
   public class GenderConverter : IValueConverter
    {
        public Style DefaultStyle { get; set; }
        public Style SelectedStyle { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is char gender)
                {
                    if (parameter is char buttonGender)
                    {
                        return GetStyle(gender, buttonGender);
                    }
                }
            }
            return DefaultStyle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        private Style GetStyle(char gender, char buttonGender)
        {
            if (gender.Equals(buttonGender))
                return SelectedStyle;
            else
                return DefaultStyle;
        }
    }
}
