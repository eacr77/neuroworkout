﻿using System;
using System.Globalization;
using Xamarin.Forms;
namespace NeuroWorkoutApp.Converts
{
    public class NullToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (parameter != null)
            {
                if (parameter is bool band)
                {
                    if (value != null)
                    {
                        return !band;
                    }
                    else
                    {
                        return band;
                    }
                }
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
