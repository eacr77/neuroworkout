﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
    public class SurveyViewModel : BaseViewModel
    {
        #region Properties      
        public TestModel Respuestas { get; set; }
        public int Padecimiento { get; set; }
        public int PuntajeFeed { get; set; }
        private bool _esInicial;

        public bool esInicial
        {
            get { return _esInicial; }
            set
            {
                _esInicial = value;
                RaisePropertyChanged(() => Error);
            }
        }
        private bool _error;

        public bool Error
        {
            get { return _error; }
            set { _error = value;
                RaisePropertyChanged(() => Error);
            }
        }
        private string _TextError;

        public string TextError
        {
            get { return _TextError; }
            set { _TextError = value;
                RaisePropertyChanged(() => TextError);
            }
        }
        private string _Testitle;

        public string Testitle
        {
            get { return _Testitle; }
            set
            {
                _Testitle = value;
                RaisePropertyChanged(() => Testitle);
            }
        }

        public int Current { get; set; }
        private int _NumPregunta;

        public int NumPregunta
        {
            get { return _NumPregunta; }
            set
            {
                _NumPregunta = value;
                RaisePropertyChanged(() => NumPregunta);
            }
        }
        private bool _ZeroButton;

        public bool ZeroButton
        {
            get { return _ZeroButton; }
            set { _ZeroButton = value;
                RaisePropertyChanged(() => ZeroButton);
            }
        }
        private bool _OneButton;

        public bool OneButton
        {
            get { return _OneButton; }
            set { _OneButton = value;
                RaisePropertyChanged(() => OneButton);
            }
        }
        private bool _TwoButton;

        public bool TwoButton
        {
            get { return _TwoButton; }
            set { _TwoButton = value;
                RaisePropertyChanged(() => TwoButton);
            }
        }
        private bool _ThreeButton;

        public bool ThreeButton
        {
            get { return _ThreeButton; }
            set { _ThreeButton = value;
                RaisePropertyChanged(() => ThreeButton);
            }
        }

        private bool _Anterior;

        public bool Anterior
        {
            get { return _Anterior; }
            set { _Anterior = value;
                RaisePropertyChanged(() => Anterior);
            }
        }
        private bool _Siguiente;

        public bool Siguiente
        {
            get { return _Siguiente; }
            set { _Siguiente = value;
                RaisePropertyChanged(() => Siguiente);
            }
        }

        private string _Question;

        public string Question
        {
            get { return _Question; }
            set { _Question = value;
                RaisePropertyChanged(() => Question);
            }
        }

        public List<PreguntaModel> Preguntas { get; set; }
        public bool ExecZeroAns { get; set; }
        public bool ExecOneAns { get; set; }
        public bool ExecTwoAns { get; set; }
        public bool ExecThreeAns { get; set; }
        public bool ExecFinish { get; set; }
        private bool _execFinish;

        public bool FinishExec
        {
            get { return _execFinish; }
            set { _execFinish = value;
                RaisePropertyChanged(() => FinishExec);
            }
        }
        private string _TextBtnZero;

        public string TextBtnZero
        {
            get { return _TextBtnZero; }
            set
            {
                _TextBtnZero = value;
                RaisePropertyChanged(() => TextBtnZero);
            }
        }
        private string _TextBtnOne;

        public string TextBtnOne
        {
            get { return _TextBtnOne; }
            set
            {
                _TextBtnOne = value;
                RaisePropertyChanged(() => TextBtnOne);
            }
        }
        private string _TextBtnTwo;

        public string TextBtnTwo
        {
            get { return _TextBtnTwo; }
            set
            {
                _TextBtnTwo = value;
                RaisePropertyChanged(() => TextBtnTwo);
            }
        }
        private string _TextBtnThree;

        public string TextBtnThree
        {
            get { return _TextBtnThree; }
            set
            {
                _TextBtnThree = value;
                RaisePropertyChanged(() => TextBtnThree);
            }
        }
        #endregion
        #region Commands
        public ICommand AnteriorCommand { get; set; }
        public ICommand SiguienteCommand { get; set; }
        public ICommand ZeroAnswer { get; set; }
        public ICommand OneAnswer { get; set; }
        public ICommand TwoAnswer { get; set; }
        public ICommand ThreeAnswer { get; set; }

        public ICommand FinishCommand { get; set; }
        #endregion
        #region Constructor
        public SurveyViewModel()
        {
                      
            
            Anterior = false;           
            AnteriorCommand = new Command(AnteriorMet, () => Anterior);
            Siguiente = true;
            SiguienteCommand = new Command(SiguienteMet, () => Siguiente);
            ZeroAnswer = new Command(ZeroAns, () => ZeroButton);
            OneAnswer = new Command(OneAns, () => OneButton);
            TwoAnswer = new Command(TwoAns, () => TwoButton);
            ThreeAnswer = new Command(ThreeAns, () => ThreeButton);
            ZeroButton = true;
            OneButton = true;
            TwoButton = true;
            ThreeButton = true;
            ExecFinish = true;
            FinishExec = false;
            FinishCommand = new Command(FinishMet,()=>ExecFinish);
          
        }
        #endregion
        #region Methods
        public void ConfigButtons()
        {
            if(esInicial)
            {
                TextBtnZero = "No me ha ocurrido";
                TextBtnOne = "Al menos una vez";
                TextBtnTwo = "Varias veces";
                TextBtnThree = "Frecuentemente";
            }
            else
            {
                TextBtnZero = "Nada";
                TextBtnOne = "Un poco";
                TextBtnTwo = "Varias veces";
                TextBtnThree = "Muchisimo";
            }
        }
        public override async Task InitializeAsync(object parameters)
        {
            try
            {

                if (parameters!=null)
                {
                    
                    Loading.Show();
                    var arri= (object[])parameters;
                    esInicial = (bool)arri[1];
                    Padecimiento = (int)arri[0];
                    Testitle = esInicial ? "DASS-21" : "Retroalimentación";
                    CargarPreguntas();
                    ConfigButtons();

                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        public async void CargarPreguntas()
        {
            try
            {
                Loading.Show();
                var peso = MainViewModel.GetInstance().usuario.Peso;
                var altura = MainViewModel.GetInstance().usuario.Altura;
                if (peso != 0 && altura != 0)
                {
                    TestConsumer test = new TestConsumer(Network);
                    int opc = esInicial == true ? 1 : 2;
                    var result = await test.GetEncuesta(opc);
                    if (result != null && result.Count != 0)
                    {
                        Preguntas = result;
                        Question = Preguntas[0].Pregunta;
                        Current = 0;
                        NumPregunta = 1;
                    }
                    else
                    {
                        await ShowAlertAsync("Información", "Ocurrió un error al cargar las preguntas del test, intente de nuevo");
                    }
                }
                else
                {
                    await ShowAlertAsync("Información", "Actualiza los datos de tu perfil para continuar");
                    await Navigation.MasterPopAsync();
                }
               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        public async void AnteriorMet()
        {
            try
            {
                CanExecuteAction(false, (Command)AnteriorCommand);
                FinishExec = false;
                if (Current > 0)
                {
                    Current--;
                    NumPregunta--;
                    Siguiente = true;
                    Question = Preguntas[Current].Pregunta;
                }
                else
                {
                    Anterior = false;
                }
                TestButtons();
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)AnteriorCommand);
            }
        }
        public async void SiguienteMet()
        {
            try
            {
                CanExecuteAction(false, (Command)AnteriorCommand);
                if (Preguntas[Current].Valor != -1)
                {
                    if (Current < Preguntas.Count - 1)
                    {
                        Current++;
                        NumPregunta++;
                        Anterior = true;
                        Question = Preguntas[Current].Pregunta;
                    }
                    else
                    {
                        Siguiente = false;
                        FinishExec = true;
                    }
                    TestButtons();
                }
                else
                {
                    await ShowAlertAsync("Información", "Responda por favor");
                }
                
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)AnteriorCommand);
            }
        }
        public void TestButtons()
        {
            switch (Preguntas[Current].Valor)
            {
                case 0:
                    {
                        ZeroButton = false;
                        OneButton = true;
                        TwoButton = true;
                        ThreeButton = true;
                    }
                    break;
                case 1:
                    {
                        ZeroButton = true;
                        OneButton = false;
                        TwoButton = true;
                        ThreeButton = true;
                    }
                    break;
                case 2:
                    {
                        ZeroButton = true;
                        OneButton = true;
                        TwoButton = false;
                        ThreeButton = true;
                    }
                    break;
                case 3:
                    {
                        ZeroButton = true;
                        OneButton = true;
                        TwoButton = true;
                        ThreeButton = false;
                    }
                    break;
                default:
                    {
                        ZeroButton = true;
                        OneButton = true;
                        TwoButton = true;
                        ThreeButton = true;
                    }
                    break;
            }
        }
        public async void ZeroAns()
        {
            try
            {
                CanExecuteAction(false, (Command)ZeroAnswer);
                ZeroButton = false;
                OneButton = true;
                TwoButton = true;
                ThreeButton = true;
                Preguntas[Current].Valor = 0;

            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)ZeroAnswer);
            }
        }
        public async void OneAns()
        {
            try
            {
                CanExecuteAction(false, (Command)OneAnswer);
                ZeroButton = true;
                OneButton = false;
                TwoButton = true;
                ThreeButton = true;
                Preguntas[Current].Valor = 1;
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)OneAnswer);
            }
        }
        public async void TwoAns()
        {
            try
            {
                CanExecuteAction(false, (Command)TwoAnswer);
                ZeroButton = true;
                OneButton = true;
                TwoButton = false;
                ThreeButton = true;
                Preguntas[Current].Valor = 2;
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)TwoAnswer);
            }
        }
        public async void ThreeAns()
        {
            try
            {
                CanExecuteAction(false, (Command)ThreeAnswer);
                ZeroButton = true;
                OneButton = true;
                TwoButton = true;
                ThreeButton = false;
                Preguntas[Current].Valor = 3;
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)ThreeAnswer);
            }
        }
        public async void FinishMet()
        {
            try
            {
                int QSinResponder = 0;
                CanExecuteAction(false, (Command)FinishCommand);
                foreach(var item in Preguntas)
                {
                    if (item.Valor == -1)
                    {
                        QSinResponder++;
                    }
                    
                }
                if (QSinResponder > 0)
                {
                    await ShowAlertAsync("Error", "Usted tiene " + QSinResponder + " preguntas sin responder");
                }
                else
                {

                    if(esInicial)
                    {
                        SurveyResult();                                               
                    }
                    else
                    {
                        FeedbackResult();                                              
                    }                   
                   
                }
                
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)FinishCommand);
            }
        }
        public async void SaveTest(TestModel test)
        {
            try
            {
                Loading.Show();
                if (test!=null)
                {
                    TestConsumer service = new TestConsumer(Network);
                    var result = await service.SetEncuesta(test);
                    if (result == 1)
                    {
                        await ShowAlertAsync("Exito", "Datos guardados");
                        if (esInicial)
                        {
                            await Navigation.MasterPushAsync(typeof(ResultViewModel), Respuestas, true);
                        }
                        else
                        {
                            await Navigation.MasterPopToRootAsync(true);
                        }

                    }
                    else
                    {
                        await ShowAlertAsync("Error", "Fallo al guardar intente de otra vez");
                    }
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        public void SurveyResult()
        {
            try
            {
                int[] ItemsDepresion = { 3, 5, 10, 13, 16, 17, 21 };
                int[] ItemsEstres = { 1, 6, 8, 11, 12, 14, 18 };
                int[] ItemsAnsiedad = { 2, 4, 7, 9, 15, 19, 20 };
                int PuntajeDepresion = 0;
                int PuntajeAnsiedad = 0;
                int PuntajeEstres = 0;
                string RDepresion = "";
                string RAnsiedad = "";
                string REstres = "";
                int NivelD = 0;
                int NivelA = 0;
                int NivelE = 0;
                List<RespuestaModel> RespuestasTest = new List<RespuestaModel>();
                foreach(var item in Preguntas)
                {
                    var itemRes = new RespuestaModel
                    {
                        IdPreguntaTest = item.IdPreguntaTest,
                        Respuesta=item.Valor
                    };
                    RespuestasTest.Add(itemRes);
                    if (Array.Exists(ItemsDepresion, e => e == item.Orden))
                    {
                        PuntajeDepresion += item.Valor;
                    }
                    else if (Array.Exists(ItemsAnsiedad, e => e == item.Orden))
                    {
                        PuntajeAnsiedad += item.Valor;
                    }
                    else if (Array.Exists(ItemsEstres, e => e == item.Orden))
                    {
                        PuntajeEstres += item.Valor;
                    }
                }
                //Rango de Depresion
                if (PuntajeDepresion < 5)
                {
                    NivelD = 0;
                    RDepresion = "Sin Depresion";
                }
                else if (PuntajeDepresion == 5 || PuntajeDepresion == 6)
                {
                    NivelD = 1;
                    RDepresion = "Leve";
                }
                else if (PuntajeDepresion >= 7 && PuntajeDepresion <= 10)
                {
                    NivelD = 2;
                    RDepresion = "Moderado";
                }
                else if (PuntajeDepresion >= 11 && PuntajeDepresion <= 13)
                {
                    NivelD = 3;
                    RDepresion = "Severo";
                }
                else if (PuntajeDepresion >= 14)
                {
                    NivelD = 4;
                    RDepresion = "Muy Severo";
                }

                if (PuntajeAnsiedad <= 3)
                {
                    NivelA = 0;
                    RAnsiedad = "Sin Ansiedad";
                }
                else if (PuntajeAnsiedad == 4)
                {
                    NivelA = 1;
                    RAnsiedad = "Leve";
                }
                else if (PuntajeAnsiedad >= 5 && PuntajeAnsiedad <= 7)
                {
                    NivelA = 2;
                    RAnsiedad = "Moderado";
                }
                else if (PuntajeAnsiedad == 8 || PuntajeAnsiedad == 9)
                {
                    NivelA = 3;
                    RAnsiedad = "Severo";
                }
                else if (PuntajeAnsiedad >= 10)
                {
                    NivelA = 4;
                    RAnsiedad = "Muy Severo";
                }

                if (PuntajeEstres <= 7)
                {
                    NivelE = 0;
                    REstres = "Sin Estres";
                }
                else if (PuntajeEstres == 8 || PuntajeEstres == 9)
                {
                    NivelE = 1;
                    REstres = "Leve";
                }
                else if (PuntajeEstres >= 10 && PuntajeEstres <= 12)
                {
                    NivelE = 2;
                    REstres = "Moderado";
                }
                else if (PuntajeEstres >= 13 && PuntajeEstres <= 16)
                {
                    NivelE = 3;
                    REstres = "Severo";
                }
                else if (PuntajeEstres >= 17)
                {
                    NivelE = 4;
                    REstres = "Muy Severa";
                }
                Respuestas = new TestModel
                {
                    IdTest = Preguntas[0].IdTest,
                    EsInicial = esInicial,
                    IdCliente=MainViewModel.GetInstance().usuario.IdUsuario,
                    NivelEstres=NivelE,
                    NivelAnsiedad=NivelA,
                    NivelDepresion=NivelD,
                    PuntajeEstres=PuntajeEstres,
                    PuntajeAnsiedad=PuntajeAnsiedad,
                    PuntajeDepresion=PuntajeDepresion,
                    ResultadoAnsiedad=RAnsiedad,
                    ResultadoEstres=REstres,
                    ResultadoDepresion=RDepresion,
                    Respuestas=RespuestasTest
                };
               SaveTest(Respuestas);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async void FeedbackResult()
        {
            try
            {
                int PuntajeTest = 0;
                int PuntajeDepresion = 0;
                int PuntajeAnsiedad = 0;
                int PuntajeEstres = 0;
                string RDepresion = "";
                string RAnsiedad = "";
                string REstres = "";
                int NivelD = 0;
                int NivelA = 0;
                int NivelE = 0;
                int level = 0;
                string msj = "";
                List<RespuestaModel> RespuestasTest = new List<RespuestaModel>();
                foreach(var item in Preguntas)
                {
                    var itemRes = new RespuestaModel
                    {
                        IdPreguntaTest = item.IdPreguntaTest,
                        Respuesta = item.Valor
                    };
                    PuntajeTest += item.Valor;
                    RespuestasTest.Add(itemRes);
                }
                PuntajeFeed = PuntajeTest;
                if(PuntajeFeed<=10 )
                {
                    level = 1;
                    msj = "Te recomendamos acudir a un profesional para una mejor atención";
                }
                else if(PuntajeFeed>10 && PuntajeFeed <= 12)
                {
                    level = 2;
                    msj = "Podrías intentar realizar el ejercicio más tarde, descansa";
                }
                else if (PuntajeFeed>12 && PuntajeFeed<=20)
                {
                    level = 3;
                    msj = "Muy bien, sigue así que tengas un buen día";
                }
                else if (PuntajeFeed > 20 && PuntajeFeed <= 27)
                {
                    level = 4;
                    msj = "Excelente, el que persevera alcanza.";
                }
                switch(Padecimiento)
                {
                    //Estres
                    case 1:
                        {
                            NivelE = level;
                            PuntajeEstres = PuntajeFeed;
                            REstres = msj;
                        }
                        break;
                    //Depresion
                    case 2:
                        {
                            NivelD = level;
                            PuntajeDepresion = PuntajeFeed;
                            RDepresion = msj;
                        }
                        break;
                    //Ansiedad
                    case 3:
                        {
                            NivelA = level;
                            PuntajeAnsiedad = PuntajeFeed;
                            RAnsiedad = msj;
                        }
                        break;
                }
                Respuestas = new TestModel
                {
                    IdTest = Preguntas[0].IdTest,
                    EsInicial = esInicial,
                    IdCliente = MainViewModel.GetInstance().usuario.IdUsuario,
                    NivelEstres = NivelE,
                    NivelAnsiedad = NivelA,
                    NivelDepresion = NivelD,
                    PuntajeEstres = PuntajeEstres,
                    PuntajeAnsiedad = PuntajeAnsiedad,
                    PuntajeDepresion = PuntajeDepresion,
                    ResultadoAnsiedad = RAnsiedad,
                    ResultadoEstres = REstres,
                    ResultadoDepresion = RDepresion,
                    Respuestas = RespuestasTest
                };
               SaveTest(Respuestas);
                await ShowAlertAsync("Información",msj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)ZeroAnswer))
                ExecZeroAns = value;
            if (command.Equals((Command)OneAnswer))
                ExecOneAns = value;
            if (command.Equals((Command)TwoAnswer))
                ExecTwoAns = value;
            if (command.Equals((Command)ThreeAnswer))
                ExecThreeAns = value;
            command.ChangeCanExecute();
        }
        #endregion
    }
}
