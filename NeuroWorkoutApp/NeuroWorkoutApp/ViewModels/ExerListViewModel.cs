﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ExerListViewModel : BaseViewModel
    {
        #region Properties
        public int TipoEjercicio { get; set; }
        private string _titulo;

        public string Titulo
        {
            get { return _titulo; }
            set { 
                _titulo = value;
                RaisePropertyChanged(() => Titulo);
            }
        }
        private List<EjercicioModel> _ejercicios;

        public List<EjercicioModel> Ejercicios
        {
            get { return _ejercicios; }
            set { 
                _ejercicios = value;
                RaisePropertyChanged(() => Ejercicios);
            }
        }

        #endregion
        #region Commands
        public ICommand SelectedExerciseCommand => new Command<EjercicioModel>(execute: async (item) => {
            EjercicioModel ejer = new EjercicioModel();
            ejer.IdEjercicio = item.IdEjercicio;
            ejer.Nombre = item.Nombre;
            await NavegarPagina(ejer);
        });
        #endregion
        #region Constructor
        public ExerListViewModel()
        {

        }
        #endregion
        #region Methods
        public override async Task InitializeAsync(object parameters)
        {
            try
            {

                if (parameters.GetType().IsArray)
                {
                    object[] arr = (object[])parameters;
                    Loading.Show();
                    TipoEjercicio = (int)arr[0];
                    switch (TipoEjercicio)
                    {
                        case 1:
                            {
                                Titulo = "Fuerza";
                               
                            }

                            break;
                        case 2:
                            {
                                Titulo = "Cardio";
                                
                            }

                            break;
                        case 3:
                            {
                                Titulo = "Calistenia";
                               
                            }
                            break;
                        case 4:
                            {
                                Titulo = "Estiramiento";
                               
                            }
                            break;
                    }
                    await LoadEjercicios(TipoEjercicio);
                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        public async Task LoadEjercicios(int tipo)
        {
            try
            {
                Loading.Show();
                TrainingConsumer services = new TrainingConsumer(Network);
                var result = await services.GetAllEjercicios(tipo);
                Ejercicios = new List<EjercicioModel>();
                if (result != null)
                {
                    if (result.Count != 0)
                    {
                        Ejercicios = result;
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "Ocurrio un problema al cargar tus datos");
                    }
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                Loading.Hide();
            }
        }
        private async Task NavegarPagina(EjercicioModel item)
        {

            await Navigation.MasterPushAsync(typeof(ExerDetViewModel), item, true);
        }
        #endregion
    }
}
