﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.ViewModels
{
   public class MainViewModel : BaseViewModel
    {
        #region Properties
        private static MainViewModel instance;
        public User usuario { get; set; }
        #endregion
        #region Constructor
        public MainViewModel()
        {
            instance = this;
        }
        #endregion
        #region Methods
        public static MainViewModel GetInstance()
        {
            if (instance == null)
                return new MainViewModel();
            return instance;
        }
        #endregion
    }
}
