﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels.Items
{
   public class MenuItemViewModel:BaseViewModel
    {
        public MenuApp menu { get; set; }
        public Page LastPage { get; set; }
        #region Command
        public ICommand SelectCommand { get; private set; }
        #endregion
        public MenuItemViewModel()
        {
            
            SelectCommand = new Command(SelectAction);
        }

        private async void SelectAction()
        {
            try
            {
                var navigationService = ServiceLocator.Resolve<INavigationService>();

                MasterDetailPage masterDetailRootPage = (MasterDetailPage)Application.Current.MainPage;
               
                if (LastPage!=App.Navigator.CurrentPage)
                {
                    switch (menu.ViewModelName)
                    {
                        case "HomeViewModel":
                            await navigationService.RootNavigation(typeof(MasterViewModel), true);
                            break;
                        case "TrainingViewModel":
                            await navigationService.MasterPushAsync(typeof(TrainingViewModel), true);
                            break;
                        case "ExerciseViewModel":
                            await navigationService.MasterPushAsync(typeof(ExerciseViewModel), true);
                            break;
                        case "StatisticsViewModel":
                            await navigationService.MasterPushAsync(typeof(StatisticsViewModel), true);
                            break;
                        case "RepositoryViewModel":
                            await navigationService.MasterPushAsync(typeof(RepositoryViewModel), true);
                            break;
                        default:
                            await navigationService.CreateNavigation(typeof(StartViewModel), true);
                            break;
                    }
                    masterDetailRootPage.IsPresented = false;                    
                }
                LastPage = App.Navigator.CurrentPage;

            }
            catch (Exception ex)
            {

                Trace.TraceError(ex.Message);
            }
        }
    }
}
