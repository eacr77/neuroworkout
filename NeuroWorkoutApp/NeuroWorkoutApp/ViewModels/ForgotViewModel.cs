﻿using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.Validations;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ForgotViewModel:BaseViewModel
    {
        private ValidatableObject<string> _email;
        //private IForgotAccountService _forgotAccountService;
        #region Properties
        public ValidatableObject<string> Email
        {
            get { return _email; }
            set { RaisePropertyChanged(() => Email); }
        }
        public bool CanExecuteSignIn { get; set; }
        public bool CanExecuteGoBack { get; set; }
        public bool CanExecuteGoToRegister { get; set; }
        #endregion
        #region Commands
        public ICommand SendCommand => new Command(execute: async () => await SignIn(), canExecute: () => CanExecuteSignIn);
        public ICommand ValidateEmailCommand => new Command(() => _email.Validate());
        #endregion
        #region Constructor
        public ForgotViewModel()
        {
            _email = new ValidatableObject<string>();
            AddValidations();
            CanExecuteSignIn = true;
        }
        #endregion
        #region Methods
        private async Task SignIn()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)SendCommand);

                if (IsValid())
                {
                    UserConsumer user = new UserConsumer(Network);
                    var result = await user.RecuperarUser(Email.Value);
                    if(result!=null)
                    {
                        await ShowAlertAsync("Correo enviado", "Le enviamos un correo con instrucciones para recuperar su contraseña");

                        _email.Value = string.Empty;

                        await Navigation.CreateNavigation(typeof(LoginViewModel), true);
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "Lo sentimos el correo proporcionado no se encontró");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ShowAlert(Messages.ErrorTitle, ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)SendCommand);
            }
        }
        private bool IsValid()
        {
            Email.Validate();

            return Email.IsValid;
        }

        private void AddValidations()
        {
            _email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su dirección de correo." });
            _email.Validations.Add(new IsValidEmailRule { ValidationMessage = "Ingrese una cuenta de correo válida." });
        }

        private void CanExecuteAction(bool value, Command command)
        {
           
            if (command.Equals((Command)SendCommand))
                CanExecuteSignIn = value;

            command.ChangeCanExecute();
        }
        #endregion
    }
}
