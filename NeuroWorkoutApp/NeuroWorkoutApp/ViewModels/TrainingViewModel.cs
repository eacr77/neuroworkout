﻿using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class TrainingViewModel:BaseViewModel
    {
        #region Properties
        public bool CanExecuteGoToTest { get; set; }
        private string _explicacion;

        public string Explicacion
        {
            get { return _explicacion; }
            set {
                _explicacion = value; 
                RaisePropertyChanged(() => Explicacion); }
        }

        #endregion
        #region Commands
        public ICommand TestCommand { get; set; }
        #endregion
        #region Constructor
        public TrainingViewModel()
        {
            CanExecuteGoToTest = true;
            Explicacion = "En NeuroWorkout queremos ofrecerte un experiencia" +
                " personalizada para tu entrenamiento, en base a tus datos fisicos " +
                "y a como te encuentras hoy te recomendaremos una rutina que se pueda adaptar a ti.\n" +
                "Utilizando el Test DASS-21 para medir tus niveles de Ansiedad, Depresión y Estrés, en base a los resultados" +
                "te sugeriremos un entrenamiento adecuado";
            TestCommand=new Command(execute: async () => await GotoTest(), canExecute: () => CanExecuteGoToTest);
        }
        #endregion
        #region Methods
        private async Task GotoTest()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)TestCommand);
                object[] valores = {0,true };
                await Navigation.MasterPushAsync(typeof(SurveyViewModel), valores, true);                
            }
            catch (Exception ex)
            {
                ShowAlert(Messages.ErrorTitle, ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)TestCommand);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {

            if (command.Equals((Command)TestCommand))
                CanExecuteGoToTest = value;

            command.ChangeCanExecute();
        }
        #endregion
    }
}
