﻿using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ExerciseViewModel:BaseViewModel
    {
        #region Properties
        public bool CanExecEst { get; set; }
        public bool CanexecCard { get; set; }
        public bool CanexecFor { get; set; }
        public bool CanexecCalis { get; set; }
        #endregion
        #region Commands
        public ICommand EstCommand { get; set; }
        public ICommand CardioCommand { get; set; }
        public ICommand ForceCommand { get; set; }
        public ICommand CalisCommand { get; set; }
        #endregion
        #region Constructor
        public ExerciseViewModel()
        {
            CanExecEst = true;
            CanexecCard = true;
            CanexecCalis = true;
            CanexecFor = true;
            EstCommand = new Command(async () => await EstAction(), () => CanExecEst);
            CardioCommand = new Command(async () => await CardioAction(), () => CanexecCard);
            ForceCommand = new Command(async () => await FuerzaAction(), () => CanexecFor);
            CalisCommand = new Command(async () => await CalisteniaAction(), () => CanexecCalis);

        }
        #endregion
        #region Methods
        private async Task CalisteniaAction()
        {
            try
            {
                CanExecuteAction(false, (Command)CalisCommand);
                object[] arr = { 3 };
                await Navigation.MasterPushAsync(typeof(ExerListViewModel), arr);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)CalisCommand);
            }
        }

        private async Task FuerzaAction()
        {
            try
            {
                CanExecuteAction(false, (Command)ForceCommand);
                object[] arr = { 1 };
                await Navigation.MasterPushAsync(typeof(ExerListViewModel), arr);

            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)ForceCommand);
            }
        }

        private async Task CardioAction()
        {
            try
            {
                CanExecuteAction(false, (Command)CardioCommand);
                object[] arr = { 2 };
                await Navigation.MasterPushAsync(typeof(ExerListViewModel), arr);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)CardioCommand);
            }
        }

        private async Task EstAction()
        {
            try
            {
                CanExecuteAction(false, (Command)EstCommand);

                object[] arr = { 4 };
                await Navigation.MasterPushAsync(typeof(ExerListViewModel), arr);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)EstCommand);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)EstCommand))
            {
                CanExecEst = value;
            }
            if (command.Equals((Command)CardioCommand))
            {
                CanexecCard = value;
            }
            if (command.Equals((Command)ForceCommand))
            {
                CanexecFor = value;
            }
            if (command.Equals((Command)CalisCommand))
            {
                CanexecCalis = value;
            }
            command.ChangeCanExecute();
        }
        #endregion
    }
}
