﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.Validations;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class RegisterViewModel:BaseViewModel
    {
        #region Properties
        #region Private
        private ValidatableObject<bool> aceptoTyC;
        private ValidatableObject<string> confirmPassword;
        private ValidatableObject<string> email;
        private ValidatableObject<string> nombreCompleto;
        private ValidatableObject<string> user;
        private ValidatableObject<string> password;
        private ValidatableObject<char> sexo;
        private ValidatableObject<DateTime> fechanac;
        #endregion
        #region Public
        public ValidatableObject<DateTime> FechaNac
        {
            get { return fechanac; }
            set
            {
                fechanac = value;
                RaisePropertyChanged(() => FechaNac);
            }
        }
        public ValidatableObject<string> Usuario
        {
            get { return user; }
            set
            {
                user = value;
                RaisePropertyChanged(() => Usuario);
            }
        }
        public ValidatableObject<bool> AceptoTyC
        {
            get { return aceptoTyC; }
            set
            {
                aceptoTyC = value;
                RaisePropertyChanged(() => AceptoTyC);
            }
        }
        public bool CanExecuteRegister { get; set; }
        public Command RegisterCommand { get; }

        public ValidatableObject<string> ConfirmPassword
        {
            get { return confirmPassword; }
            set
            {
                confirmPassword = value;
                RaisePropertyChanged(() => ConfirmPassword);
            }
        }
        public ValidatableObject<string> Email
        {
            get { return email; }
            set
            {
                email = value;
                RaisePropertyChanged(() => Email);
            }
        }
        public ValidatableObject<string> NombreCompleto
        {
            get { return nombreCompleto; }
            set
            {
                nombreCompleto = value;
                RaisePropertyChanged(() => NombreCompleto);
            }
        }
        public ValidatableObject<string> Password
        {
            get { return password; }
            set
            {
                password = value;
                RaisePropertyChanged(() => Password);
            }
        }
        public ValidatableObject<char> Sexo
        {
            get { return sexo; }
            set
            {
                sexo = value;
                RaisePropertyChanged(() => Sexo);
            }
        }
        #endregion
        #endregion
        #region Commands
        public ICommand ChangeGenderCommand => new Command<char>((x) => ChangeGender(x));
        public ICommand ValidateUserCommand => new Command(() => user.Validate());
        public ICommand ValidateFullNameCommand => new Command(() => nombreCompleto.Validate());
        public ICommand ValidateEmailCommand => new Command(() => email.Validate());
        public ICommand ValidatePasswordCommand => new Command(() => { password.Validate(); confirmPassword.Validate(); });
        public ICommand ValidateConfirmarPasswordCommand => new Command(() => { confirmPassword.Validate(); });
        public ICommand ValidateAceptoTyCCommand => new Command(() => { aceptoTyC.Validate(); });
        public ICommand TermsCommand { get; private set; }
        #endregion
        #region Constructor
        public RegisterViewModel()
        {
            InitProperties();
            AddValidations();
            CanExecuteRegister = true;
            TermsCommand = new Command(async()=>await OpenBrowser());
            RegisterCommand = new Command(async () => await RegisterActionAsync(), () => CanExecuteRegister);
        }
        #endregion
        #region Methods
        private void InitProperties()
        {
            user = new ValidatableObject<string>();
            nombreCompleto = new ValidatableObject<string>();
            email = new ValidatableObject<string>();
            password = new ValidatableObject<string>();
            confirmPassword = new ValidatableObject<string>();
            aceptoTyC = new ValidatableObject<bool>();
            sexo = new ValidatableObject<char>();
            fechanac = new ValidatableObject<DateTime>();
            FechaNac.Value = DateTime.Now;
        }
        private void ChangeGender(char newGender)
        {
            sexo.Value = newGender;
            sexo.Validate();
        }
        public async Task OpenBrowser()
        {
            try
            {
                await Browser.OpenAsync("https://drive.google.com/file/d/11rX3V3gy4-h7uV0NXM8W_eBVPhQArumD/view?usp=sharing", BrowserLaunchMode.SystemPreferred);
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error","Hubo un problema al intentar visulizar la pagina");
            }
        }
        private bool IsValid()
        {
            Usuario.Validate();
            nombreCompleto.Validate();
            email.Validate();
            password.Validate();
            confirmPassword.Validate();
            sexo.Validate();
            aceptoTyC.Validate();
            fechanac.Validate();
            return Usuario.IsValid && nombreCompleto.IsValid && email.IsValid && password.IsValid && confirmPassword.IsValid && fechanac.IsValid &&sexo.IsValid && aceptoTyC.IsValid;
        }

        private void AddValidations()
        {
            nombreCompleto.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su nombre." });
            user.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su usuario" });
            email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su dirección de correo." });
            email.Validations.Add(new IsValidEmailRule { ValidationMessage = "Ingrese una cuenta de correo válida." });

            password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su contraseña." });
            password.Validations.Add(new IsValidPasswordRule { ValidationMessage = "Ingrese una contraseña válida.\r\nDebe contener al menos un número, una mayúscula, una minúscula.\r\nDebe contener una longitud de 8 a 16 caracteres." });
            fechanac.Validations.Add(new FechaValidator { ValidationMessage = "Seleccione una fecha válida" });
            confirmPassword.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Confirme su contraseña." });
            confirmPassword.Validations.Add(new IsEqualsThanRule<string> { ValidationMessage = "Las contraseñas no coinciden.", CompareTo = password });

           sexo.Validations.Add(new IsValidGenderRule { ValidationMessage = "Indique un género." });

            aceptoTyC.Validations.Add(new IsCheckedRule { ValidationMessage = "Debe aceptar términos y condiciones." });
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)RegisterCommand))
                CanExecuteRegister = value;
            command.ChangeCanExecute();
        }
        private async Task RegisterActionAsync()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)RegisterCommand);
                if (IsValid())
                {
                    User usuario = new User
                    {
                        NombreCompleto = NombreCompleto.Value,
                        FechaNacimiento = DateTime.Now,
                        Peso = 0,
                        Altura = 0,
                        Imc = 0,
                        Telefono = "",
                        Correo = Email.Value,
                        Usuario = Usuario.Value,
                        Contraseña = Password.Value,
                        Sexo = Sexo.Value
                    };
                    UserConsumer servicio = new UserConsumer(Network);
                    var result = await servicio.Registrar(usuario);
                    if (result.Equals(1))
                    {
                        await ShowAlertAsync("Exito", "Registro exitoso. Accede con tus datos proporcionados");
                        await Application.Current.MainPage.Navigation.PopAsync();
                    }
                    else if(result==2)
                    {
                        await ShowAlertAsync("Error", "El correo ya fue registrado");
                    }
                    else if (result==3)
                    {
                        await ShowAlertAsync("Error", "El usuario que ingresaste ya existe, escribe otro");
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "No se pudo completar tu registro");
                    }
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)RegisterCommand);
            }
        }
        #endregion
    }
}
