﻿using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models.Base;
using NeuroWorkoutApp.Services;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.ViewModels.Base
{
   public class BaseViewModel : ExtendedBindableObject
    {
        protected INetworkService Network { get; set; }
        protected ILoadingService Loading { get; set; }
        protected IDialogService Dialog { get; set; }
        protected INavigationService Navigation { get; set; }

        public BaseViewModel()
        {
            InitializeInstances();
        }

        private void InitializeInstances()
        {
            Dialog = ServiceLocator.Resolve<IDialogService>();
            Loading = ServiceLocator.Resolve<ILoadingService>();
            Network = ServiceLocator.Resolve<INetworkService>();
            Navigation = ServiceLocator.Resolve<INavigationService>();
        }

        public virtual Task InitializeAsync(object parameters)
        {

            return Task.FromResult(true);
        }

        #region Network Methods

        protected bool CheckInternet()
        {
            if (!Network.IsConnected)
            {
                ShowAlertAsync(Messages.ErrorTitle, Messages.NoInternetError);
                return false;
            }
            return true;
        }

        protected bool CheckInternetWithouAlert()
        {
            return !Network.IsConnected;
        }

        protected bool CheckErrors(BaseEntity baseEntity, string title = Messages.ErrorTitle)
        {
            if (baseEntity == null)
            {
                ShowAlertAsync(title, Messages.GeneralErrorMessage);
                return true;
            }

            if (!string.IsNullOrEmpty(baseEntity.Message))
            {
                ShowAlertAsync(title, baseEntity.Message);
                return true;
            }

            return false;
        }

        #endregion

        #region Dialog Methods

        protected Task ShowAlertAsync(string title, string message, string cancel = Messages.Ok)
        {
            return Dialog.ShowAlertAsync(title, message, cancel);
        }

        protected void ShowAlert(string title, string message, string cancel = Messages.Ok)
        {
            Dialog.ShowAlert(title, message, cancel);
        }

        protected async Task<bool> ShowAlertConfirmAsync(string title, string message, string accept = Messages.Yes, string cancel = Messages.No)
        {
            return await Dialog.ShowConfirmAsync(title, message, accept, cancel);
        }

        protected async Task<string> ShowAlertSheetAsync(string title, string cancel, string destruction, params string[] options)
        {
            return await Dialog.ShowAlertSheetAsync(title, cancel, destruction, options);
        }

        protected void ShowToast(string message, int duration = 3000, bool topPosition = false, string image = null, string textColor = "#FFFFFF", string backgroundColor = "#D3D3D3")
        {
            Dialog.ShowToast(message, duration, topPosition, image, textColor, backgroundColor);
        }

        #endregion
    }
}
