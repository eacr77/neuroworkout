﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.ViewModels
{
    public class ExerDetViewModel : BaseViewModel
    {
        #region Properties
        public string Defaultimage { get; set; }
        private string _nombre;

        public string Nombre
        {
            get { return _nombre; }
            set
            {
                _nombre = value;
                RaisePropertyChanged(() => Nombre);
            }
        }
        private string _imgUrl;

        public string ImgUrl
        {
            get { return _imgUrl; }
            set
            {
                _imgUrl = value;
                RaisePropertyChanged(() => ImgUrl);
            }
        }
        private string _descripcion;

        public string Descripcion
        {
            get { return _descripcion; }
            set
            {
                _descripcion = value;
                RaisePropertyChanged(() => Descripcion);
            }
        }
        #endregion
        #region Constructor
        public ExerDetViewModel()
        {
             Defaultimage = "https://thumbs.gfycat.com/AssuredDisloyalFlies-small.gif";
        }
        #endregion
        #region Methods
        public async Task LoadEjercicio(Guid idEjercicio)
        {
            try
            {
                Loading.Show();
                TrainingConsumer services = new TrainingConsumer(Network);
                var result = await services.GetEjercicioXId(idEjercicio);
               
                if (result != null)
                {
                    Nombre = result.Nombre;
                    Descripcion = result.Descripcion;

                    var srthtm = result.UrlMedia != string.Empty ? result.UrlMedia : Defaultimage;
                    var video= "<html><head></head><body style='margin: 0; padding: 0;background:#FFFFFF;'><iframe style='height:100%;width:auto;display:block;margin:auto;' src='" + srthtm + "'/></body></html>";
                    var imagen= "<html><head></head><body style='margin: 0; padding: 0;background:#FFFFFF;'><img style='height:100%;width:auto;display:block;margin:auto;' src='" + srthtm + "'/></body></html>";
                    ImgUrl = result.EsVideo ? video : imagen;
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                Loading.Hide();
            }
        }
        public override async Task InitializeAsync(object parameters)
        {
            try
            {
                if (parameters is EjercicioModel)
                {
                    Loading.Show();
                    var model = (EjercicioModel)parameters;
                    /*Nombre = model.Nombre;
                    Descripcion = model.Descripcion;
                    ImgUrl = "<html><head></head><body style='margin: 0; padding: 0;background:#FFFFFF;'><img style='height:100%;width:auto;display:block;margin:auto;' src='" + model.UrlMedia + "'/></body></html>";*/
                    await LoadEjercicio(model.IdEjercicio);
                    Loading.Hide();
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
        }
        #endregion
    }   
}
