﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ExerTrainingViewModel:BaseViewModel
    {
        #region Properties
        public string Defaultimage { get; set; }
        public int TipoWod { get; set; }
        private string _btnPause;

        public string Btnpause
        {
            get { return _btnPause; }
            set
            {
                _btnPause = value;
                RaisePropertyChanged(() => Btnpause);
            }
        }
        private List<EjercicioWodModel> _Ejercicios;

        public List<EjercicioWodModel> Ejercicios
        {
            get { return _Ejercicios; }
            set
            {
                _Ejercicios = value;
                RaisePropertyChanged(() => Ejercicios);
            }
        }
        private string _ejercicio;

        public string NombreEjercicio
        {
            get { return _ejercicio; }
            set
            {
                _ejercicio = value;
                RaisePropertyChanged(() => NombreEjercicio);
            }
        }

        private string _tiempo;

        public string Tiempo
        {
            get { return _tiempo; }
            set
            {
                _tiempo = value;
                RaisePropertyChanged(() => Tiempo);
            }
        }
        private string _tiempo2;

        public string Tiempo2
        {
            get { return _tiempo2; }
            set
            {
                _tiempo2 = value;
                RaisePropertyChanged(() => Tiempo2);
            }
        }
        public int Segundos { get; set; }
        public int Segundos2 { get; set; }
        public int Minutos { get; set; }
        public int Minutos2 { get; set; }
        public int TotalTiempoSeg { get; set; }
        public int TotalTimpoWod { get; set; }
        private int _reps;

        public int Repeticiones
        {
            get { return _reps; }
            set
            {
                _reps = value;
                RaisePropertyChanged(() => Repeticiones);
            }
        }
        private int _metros;

        public int Metros
        {
            get { return _metros; }
            set
            {
                _metros = value;
                RaisePropertyChanged(() => Metros);
            }
        }
        private string _conejer;

        public string ContEjercicio
        {
            get { return _conejer; }
            set
            {
                _conejer = value;
                RaisePropertyChanged(() => ContEjercicio);
            }
        }
        public int CurrentEjercicio { get; set; }
        public int TotalEjercicios { get; set; }
        private string _Round;

        public string ContRound
        {
            get { return _Round; }
            set
            {
                _Round = value;
                RaisePropertyChanged(() => ContRound);
            }
        }
        public int CurrentRound { get; set; }
        public int TotalRound { get; set; }
        private bool _IsMetros;

        public bool IsMetros
        {
            get { return _IsMetros; }
            set
            {
                _IsMetros = value;
                RaisePropertyChanged(() => IsMetros);
            }
        }
        public bool Finished { get; set; }
        public bool Pause { get; set; }
        public bool Stop { get; set; }
        private bool _finish;

        public bool BtFinish
        {
            get { return _finish; }
            set { 
                _finish = value;
                RaisePropertyChanged(() => BtFinish);
            }
        }

        private string _imgUrl;

        public string ImgUrl
        {
            get { return _imgUrl; }
            set
            {
                _imgUrl = value;
                RaisePropertyChanged(() => ImgUrl);
            }
        }
        private int _valorBarra;

        public int ValorBarra
        {
            get { return _valorBarra; }
            set
            {
                _valorBarra = value;
                RaisePropertyChanged(() => ValorBarra);
            }
        }
        private decimal _progreso;

        public decimal Progreso
        {
            get { return _progreso; }
            set
            {
                _progreso = value;
                RaisePropertyChanged(() => Progreso);
            }
        }
        public int IndiceActual { get; set; }
        public Stopwatch rel;
        public bool CanexecForward { get; set; }
        public bool CanexecRewind { get; set; }
        public bool CanexecStart { get; set; }
        public bool CanexecStop { get; set; }
        public bool CanexecBack { get; set; }
        public bool CanexecFinish { get; set; }

        public bool inicarvow { get; set; }
       

        #endregion
        #region Command
        public ICommand StartCommand { get; set; }
        public ICommand FinishCommand { get; set; }
        public ICommand StopCommand { get; set; }
        public ICommand ForwardCommand { get; set; }
        public ICommand RewindCommand { get; set; }
        public ICommand BackCommand { get; set; }
        #endregion
        #region Constructor
        public ExerTrainingViewModel()
        {
            Defaultimage = "https://thumbs.gfycat.com/AssuredDisloyalFlies-small.gif";
            CanexecStart = true;
            CanexecStop = true;
            CanexecForward = true;
            CanexecRewind = true;
            CanexecBack = true;
            CanexecFinish = true;
            StartCommand = new Command(async () => {
                 if (inicarvow)
                 {
                    //await SpeakNow("Iniciamos en 3... 2... 1... ¡Vamos!");
                     Cronometro();
                     inicarvow = false;
                     CanexecStop = true;
                     CanexecStart = false;
                }
                else
                {
                    //await SpeakNow("Reanudamos en 3... 2... 1... ¡Vamos!");
                }
                
                Pause = false;
                rel.Start();
             }, () => CanexecStart);
             StopCommand = new Command(() =>  pausarWod(), () => CanexecStop);
            FinishCommand = new Command(() => FinWod(), () => CanexecFinish);
            ForwardCommand = new Command(() => {
                 CanexecRewind = true;
                 Forward();
             }, () => CanexecForward);
             RewindCommand = new Command(() => Rewind(), () => CanexecRewind);
            BackCommand = new Command(() => backwod(), () => CanexecBack);

        }
        #endregion
        #region Methods
        public override async Task InitializeAsync(object parameters)
        {
            try
            {
                
                if (parameters.GetType().IsArray)
                {
                    object[] arr = (object[])parameters;
                    Loading.Show();
                    var listejercicios = (List<EjercicioWodModel>)arr[0];
                    TipoWod = (int)arr[1];
                    TotalRound = (int)arr[2];
                    Ejercicios = listejercicios;
                    TotalEjercicios = Ejercicios.Count;
                    InicializarWod();                  
                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        public async void backwod()
        {
            try
            {
                CanExecuteAction(false, (Command)BackCommand);
                if(rel!=null)
                rel.Stop();
                Pause = true;
                CanexecStart = true;
                CanexecStop = false;
                await Navigation.MasterPopAsync();
                await ShowAlertAsync("Neuroworkout", "Wod detenido");

            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)BackCommand);
            }
        }
        public async void InicializarWod()
        {
            try
            {
                Minutos = 0;
                Segundos = 0;
                Minutos2 = 0;
                Segundos2 = 0;
                CurrentRound = 1;
                Tiempo = String.Format("{0:00}:{1:00}", Minutos, Segundos);
                Tiempo2 = String.Format("{0:00}:{1:00}", Minutos2, Segundos2);
                ContEjercicio = String.Format("{0}/{1}", CurrentEjercicio, TotalEjercicios);
                ContRound = String.Format("{0}/{1}", CurrentRound, TotalRound);
                Btnpause = "pause";
                Repeticiones = 0;
                IsMetros = false;
                Metros = 0;
                Finished = false;
                Pause = false;
                IndiceActual = 0;
                TotalTimpoWod = 0;
                NombreEjercicio = "";
                BtFinish = false;
                inicarvow = true;
                UpdateWod(IndiceActual);
                foreach (var item in Ejercicios)
                {
                    TotalTimpoWod += item.Tiempo;
                }
               
                await ShowAlertAsync("Neuroworkout", "Todo listo para entrenar presiona Play");
                //SpeakNow("Toca en Play cuando estés listo");
               
               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }

        }
        public async void Forward()
        {
            try
            {
                CanExecuteAction(false, (Command)ForwardCommand);

                if (CurrentRound < TotalRound && CurrentEjercicio == TotalEjercicios)
                {
                    IndiceActual = 0;
                    CurrentRound++;
                    //await SpeakNow("Round " + CurrentRound);
                    UpdateWod(IndiceActual);
                }else if (IndiceActual < TotalEjercicios - 1)
                {
                    IndiceActual++;
                    UpdateWod(IndiceActual);
                }else
                {
                    CanexecForward = false;
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)ForwardCommand);
            }

        }
        public async void Rewind()
        {
            try
            {

                CanExecuteAction(false, (Command)RewindCommand);
                if (IndiceActual > 0)
                {
                    IndiceActual--;
                    UpdateWod(IndiceActual);
                }
                else
                {
                    CanexecRewind = false;
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)RewindCommand);
            }
        }
        public async void UpdateWod(int indice)
        {
            if (Ejercicios.Count != 0)
            {
                if (indice < Ejercicios.Count)
                {
                    NombreEjercicio = Ejercicios[indice].Nombre;
                    var strhmt = Ejercicios[indice].UrlMedia != string.Empty ? Ejercicios[indice].UrlMedia : Defaultimage;
                    var video = "<html><head></head><body style='margin: 0; padding: 0;background:#FFFFFF;'><iframe style='height:100%;width:auto;display:block;margin:auto;' src='" + strhmt + "'/></body></html>";
                    var imagen = "<html><head></head><body style='margin: 0; padding: 0;background:#FFFFFF;'><img style='height:100%;width:auto;display:block;margin:auto;' src='" + strhmt + "'/></body></html>";
                    ImgUrl = Ejercicios[indice].EsVideo ? video : imagen;
                    if (Ejercicios[indice].BandCantidad)
                    {
                        Repeticiones = Ejercicios[indice].Cantidad;
                    }
                    if (Ejercicios[indice].BandDistancia)
                    {
                        Metros = Ejercicios[indice].Metros;
                    }
                    if (Ejercicios[indice].BandTiempo)
                    {
                        Minutos2 = Ejercicios[indice].Tiempo / 60;
                        Segundos2 = Ejercicios[indice].Tiempo % 60;                       
                    }
                    else
                    {
                        Minutos2 = 0;
                        Segundos2 = 0;
                        //if (!inicarvow && CurrentEjercicio<TotalEjercicios)
                            //await SpeakNow("Cambia el ejercicio cuando termines");
                    }
                    
                    CurrentEjercicio = indice + 1;
                    ContEjercicio = String.Format("{0}/{1}", CurrentEjercicio, TotalEjercicios);
                    if (CurrentEjercicio == TotalEjercicios && TipoWod==3 &&CurrentRound==TotalRound)
                    {
                        //await SpeakNow("¡Vamos! ya estás por finalizar");
                    }
                    Progreso = Convert.ToDecimal(CurrentEjercicio) / Convert.ToDecimal(TotalEjercicios);
                   
                   
                   
                    //if(!inicarvow)
                    //await SpeakNow(Ejercicios[indice].Nombre);
                }

            }
        }
        public async void pausarWod()
        {
            try
            {
                CanExecuteAction(false, (Command)StopCommand);
                if (Pause)
                {
                    Btnpause = "stop";
                    Stop = true;
                    Pause = false;
                    InicializarWod();
                    //SpeakNow("Entrenamiento detenido");
                }
                else
                {
                    Btnpause = "pause";
                    //SpeakNow("Entrenamiento pausado");
                }
                Pause = true;
                rel.Stop();
                CanexecStart = true;
                CanexecStop = false;

            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)StopCommand);
            }
        }
        public async void Cronometro()
        {
            try
            {
                int tiempoejer = 0;
                rel = new Stopwatch();
                //Corre mientras no sea finalizado
                while (!Finished)
                {
                    if (!Pause)
                    {
                        Segundos++;
                        //Cambia el sentido del contador de tiempo de ejercicio
                        if (Ejercicios[IndiceActual].BandTiempo && Segundos2 > 0)
                        {

                            Segundos2--;
                        }
                        else
                        {
                            Segundos2++;
                        }
                        TotalTiempoSeg++;
                        tiempoejer++;
                        // Progreso = Convert.ToDecimal(TotalTiempoSeg) / Convert.ToDecimal(TotalTimpoWod);
                        //Finaliza en un ejercicio que tenga tiempo y sea el ultimo, cuando los segundos sean 0
                        if (Ejercicios[IndiceActual].BandTiempo && CurrentEjercicio == TotalEjercicios && Minutos2 == 0 && Segundos2 == 0 && CurrentRound == TotalRound)
                        {
                            Finished = true;
                        }
                       
                        //Cambia el ejercicio automaticamente si trae tiempo
                        BtFinish = Progreso == 1 && TotalTiempoSeg >= 60 &&CurrentRound==TotalRound? true : false;
                        if (Segundos2 == 0 &&Minutos2==0&& Ejercicios[IndiceActual].Tiempo != 0 && IndiceActual <= TotalEjercicios - 1)
                        {
                            IndiceActual++;
                            if (CurrentRound < TotalRound&&CurrentEjercicio == TotalEjercicios)
                            {
                                IndiceActual = 0;
                                CurrentRound++;
                                //await SpeakNow("Round " + CurrentRound);
                            }
                            UpdateWod(IndiceActual);
                            tiempoejer = 0;                           
                        }
                       
                        //Cambio de tiempo principal
                        if (Segundos == 60)
                        {
                            Minutos++;
                            Segundos = 0;
                        }
                        //Cambio de tiempo de ejercicio normal
                        if (Segundos2 == 60 && !Ejercicios[IndiceActual].BandTiempo)
                        {
                            Minutos2++;
                            Segundos2 = 0;
                        }
                        //Cambio de tiempo si es mayor a 1 minuto
                       
                        if (Segundos2 == 0 &&Minutos2!=0&& Ejercicios[IndiceActual].BandTiempo)
                        {
                            Minutos2--;
                            Segundos2 = 59;
                        }
                        Tiempo = String.Format("{0:00}:{1:00}", Minutos, Segundos);
                        Tiempo2 = String.Format("{0:00}:{1:00}", Minutos2, Segundos2);
                        ContRound = String.Format("{0}/{1}", CurrentRound, TotalRound);
                        await Task.Delay(0);
                    }
                    
                    else if (Stop)
                    {
                        Progreso = 0;
                        TotalTiempoSeg = 0;
                        Minutos = 0;
                        Segundos = 0;
                        Minutos2 = 0;
                        Segundos2 = 0;                       
                        Tiempo = String.Format("{0:00}:{1:00}", Minutos, Segundos);
                        Tiempo2 = String.Format("{0:00}:{1:00}", Minutos2, Segundos2);
                    }                    
                    await Task.Delay(1000);
                }
              
               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", "Lo sentimos hubo un error inesperado");
            }
        }
        //public async Task SpeakNow(string text)
        //{
        //    try
        //    {
        //        var locales = await TextToSpeech.GetLocalesAsync();

        //        // Grab the first locale
                
        //        if (locales != null)
        //        {
        //            var locale = locales.Where(x => x.Name == "español (Estados Unidos)");
        //            var settings = new SpeechOptions()
        //            {
        //                Volume = .75f,
        //                Pitch = 1.0f,
        //                Locale = locale.First()
        //            };

        //            await TextToSpeech.SpeakAsync(text, settings);
        //        }
        //        else
        //        {
        //            await ShowAlertAsync("Información", "Cambia la configuración del motor tts en tu dispositivo a GoogleTTS");
        //        }
                
        //    }
        //    catch (Exception ex)
        //    {

        //        await ShowAlertAsync("Información", "Cambia la configuración del motor tts en tu dispositivo a GoogleTTS");
        //    }
            
        //}
        public async void FinWod()
        {
            try
            {
                if(rel!=null)
                {
                    //Finaliza el wod 
                    Finished = true;
                    inicarvow = true;
                    //SpeakNow("Excelente, sigue así");
                    await ShowAlertAsync("NeuroWorkout", "Etapa de entrenamiento finalizada");
                    //Manda un mensaje para actualizar la lista de entrenamientos
                    MessagingCenter.Send<ExerTrainingViewModel, int>(this, "UpdateWod", TipoWod);
                    //Fin del timer
                    rel.Stop();
                    //Remueve la pagina de info del wod y regresa a la lista de entrenamientos
                    await Navigation.RemoveLastFromBackStackAsync();
                    await Navigation.MasterPopAsync();
                }
                else
                {
                    await ShowAlertAsync("NeuroWorkout", "Debes de comenzar el entrenamiento.");
                }
                
            }
            catch (Exception ex)
            {

                
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)StartCommand))
                CanexecStart = value;
            if (command.Equals((Command)StopCommand))
                CanexecStop = value;
            if (command.Equals((Command)RewindCommand))
                CanexecRewind = value;
            if (command.Equals((Command)ForwardCommand))
                CanexecForward = value;
            if (command.Equals((Command)BackCommand))
                CanexecBack = value;
            if (command.Equals((Command)FinishCommand))
                CanexecFinish = value;
            command.ChangeCanExecute();
        }
        #endregion
    }
}
