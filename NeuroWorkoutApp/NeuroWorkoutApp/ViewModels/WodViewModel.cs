﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.Validations;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class WodViewModel:BaseViewModel
    {
        #region Properties
        public WodCompleto Wod { get; set; }
        public string FechaWod { get; set; }
        public int TipoWodAsginado { get; set; }
        public Guid IdEstiramiento { get; set; }
        public Guid IdCalentamiento { get; set; }
        public Guid IdWod { get; set; }
        public Guid IdEstiramiento2 { get; set; }

        private bool _feedback;

        public bool Feedback
        {
            get { return _feedback; }
            set { 
                _feedback = value;
                RaisePropertyChanged(() => Feedback);
            }
        }

        private string _EstInit;

        public string EstInit
        {
            get { return _EstInit; }
            set
            {
                _EstInit = value;
                RaisePropertyChanged(() => EstInit);
            }
        }
        private string _Calent;

        public string Calent
        {
            get { return _Calent; }
            set
            {
                _Calent = value;
                RaisePropertyChanged(() => Calent);
            }
        }
        private string _EstFin;

        public string EstFin
        {
            get { return _EstFin; }
            set
            {
                _EstFin = value;
                RaisePropertyChanged(() => EstFin);
            }
        }
        private string _Wod;

        public string Wood
        {
            get { return _Wod; }
            set
            {
                _Wod = value;
                RaisePropertyChanged(() => Wood);
            }
        }
        private string _titulo;

        public string Titulo
        {
            get { return _titulo; }
            set { _titulo = value;
                RaisePropertyChanged(() => Titulo);
            }
        }
        private string _nivel;

        public string Nivel
        {
            get { return _nivel; }
            set
            {
                _nivel = value;
                RaisePropertyChanged(() => Nivel);
            }
        }
        private string _estimulacion;

        public string Estimulacion
        {
            get { return _estimulacion; }
            set
            {
                _estimulacion = value;
                RaisePropertyChanged(() => Estimulacion);
            }
        }
        private string _fecha;

        public string Fecha
        {
            get { return _fecha; }
            set
            {
                _fecha = value;
                RaisePropertyChanged(() => Fecha);
            }
        }
        private int _minutos;

        public int Minutos
        {
            get { return _minutos; }
            set { 
                _minutos = value;
                RaisePropertyChanged(() => Minutos);
            }
        }
        public bool CanExecEsinit { get; set; }
        public bool CanExecCalent { get; set; }
        public bool CanExecWod { get; set; }
        public bool CanExecEstfin { get; set; }
        public bool CanExecFinish { get; set; }
        #endregion
        #region Commands
        public ICommand EstInitCommand { get; private set; }
        public ICommand CalentCommand { get; private set; }
        public ICommand WodCommand { get; private set; }
        public ICommand EstFinCommand { get; private set; }
        public ICommand FinishCommand { get; private set; }
        #endregion
        #region Constructor
        public WodViewModel()
        {
            CanExecEsinit = true;
            CanExecCalent = true;
            CanExecWod = true;
            CanExecEstfin = true;
            CanExecFinish = true;
          //  ObtenerWodPreview();
            EstInitCommand = new Command(async () => await EstInitAction(), () => CanExecEsinit);
            CalentCommand = new Command(async () => await CalentAction(), () => CanExecCalent);
            WodCommand = new Command(async () => await WodAction(), () => CanExecWod);
            EstFinCommand = new Command(async () => await EstFinAction(), () => CanExecEstfin);
            FinishCommand = new Command(async () => await FinishAction(), () => CanExecFinish);
            MessagingCenter.Subscribe<ExerTrainingViewModel, int>(this, "UpdateWod",  (sender, arg) =>
            {
                UpdateWod(arg);
            });
        }
        #endregion
        #region Methods
        public async Task ObtenerWodPreview()
        {
            try
            {
                Loading.Show();
                //FechaWod = "2021-04-16";
                Wod = new WodCompleto();
                TrainingConsumer servicio = new TrainingConsumer(Network);
                var result = await servicio.GetWodTest(TipoWodAsginado);
                if (result != null)
                {

                    Wod = result;
                    Titulo = result.NombreWod;
                    Nivel = result.Nivel;
                    Minutos = result.TiempoTotalMinutos;
                    Estimulacion = result.TipoEstimulacion;
                    IdEstiramiento = result.IdEstiramiento;
                    IdCalentamiento = result.IdCalentamiento;
                    IdEstiramiento2 = result.IdEstiramiento2;
                    IdWod = result.Id;
                    
                    EstInit = "cancel";
                    Calent = "cancel";
                    EstFin = "cancel";
                    Wood = "cancel";
                    Feedback = false;
                }
                else
                {
                    await ShowAlertAsync("Error", "Ocurrio un problema al cargar tus datos");
                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        private async Task EstFinAction()
        {
            try
            {
                CanExecuteAction(false, (Command)EstFinCommand);
                Wod.TipoWod = 4;
                await Navigation.MasterPushAsync(typeof(RoutineViewModel), Wod);               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)EstFinCommand);
            }
        }

        private async Task WodAction()
        {
            try
            {
                CanExecuteAction(false, (Command)WodCommand);
                Wod.TipoWod = 3;
                await Navigation.MasterPushAsync(typeof(RoutineViewModel), Wod);
               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)WodCommand);
            }
        }

        private async Task CalentAction()
        {
            try
            {
                CanExecuteAction(false, (Command)CalentCommand);
                Wod.TipoWod = 2;
                await Navigation.MasterPushAsync(typeof(RoutineViewModel), Wod);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)CalentCommand);
            }
        }

        private async Task EstInitAction()
        {
            try
            {
                CanExecuteAction(false, (Command)EstInitCommand);
                Wod.TipoWod = 1;
                await Navigation.MasterPushAsync(typeof(RoutineViewModel),Wod,true);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)EstInitCommand);
            }
        }
        private async Task FinishAction()
        {
            try
            {
                CanExecuteAction(false, (Command)FinishCommand);
                Wod.TipoWod = 4;
                object[] valores = { TipoWodAsginado, false };
                await Navigation.MasterPushAsync(typeof(SurveyViewModel), valores, true);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)FinishCommand);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)EstInitCommand))
                CanExecEsinit = value;
            if (command.Equals((Command)CalentCommand))
                CanExecCalent = value;
            if (command.Equals((Command)WodCommand))
                CanExecWod = value;
            if (command.Equals((Command)EstFinCommand))
                CanExecEstfin = value;
            if (command.Equals((Command)FinishCommand))
                CanExecFinish = value;
            command.ChangeCanExecute();
        }
        public void UpdateWod(int tipoWod)
        {
            switch (tipoWod)
            {
                case 1:
                    {
                        EstInit = "checked";
                        CanExecuteAction(false, (Command)EstInitCommand);
                    }
                    break;
                case 2:
                    {
                        Calent = "checked";
                        CanExecuteAction(false, (Command)CalentCommand);
                    }
                    break;
                case 3:
                    {
                        Wood = "checked";
                        CanExecuteAction(false, (Command)WodCommand);
                    }
                    break;
                case 4:
                    {
                        EstFin = "checked";
                        CanExecuteAction(false, (Command)EstFinCommand);
                    }
                    break;                
            }
           
            if (EstFin=="checked")
            {
                Feedback = true;
            }
        }
        public override async Task InitializeAsync(object parameters)
        {
            try
            {

                if (parameters!=null)
                {
                   
                    Loading.Show();
                    TipoWodAsginado = (int)parameters;
                    await ObtenerWodPreview();
                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        #endregion
    }
}
