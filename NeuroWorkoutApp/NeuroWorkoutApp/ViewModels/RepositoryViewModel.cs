﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class RepositoryViewModel:BaseViewModel
    {
        #region Properties
        private List<ArticleModel> _articulos;

        public List<ArticleModel> Articulos
        {
            get { return _articulos; }
            set
            {
                _articulos = value;
                RaisePropertyChanged(() => Articulos);
            }
        }
        #endregion
        #region Commands
        public ICommand SelectedArticleCommand => new Command<ArticleModel>(execute: async (item) => {

            
            await Browser.OpenAsync(item.UrlArchivo, new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("174188"),
                PreferredControlColor = Color.FromHex("174188")
            });

        });
        #endregion
        #region Constructor
        public RepositoryViewModel()
        {
            LoadArticulos();
        }
        #endregion
        #region Methods
        public async void LoadArticulos()
        {
            try
            {
                Loading.Show();
                ArticleConsumer services = new ArticleConsumer(Network);
                var result = await services.GetAllArticles();
                Articulos = new List<ArticleModel>();
                if (result != null)
                {
                    if (result.Count != 0)
                    {
                        Articulos = result;
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "Ocurrio un problema al cargar los datos");
                    }
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                Loading.Hide();
            }
        }
        #endregion
    }
}
