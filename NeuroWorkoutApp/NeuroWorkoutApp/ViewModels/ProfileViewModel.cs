﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.Validations;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ProfileViewModel:BaseViewModel
    {
        #region Properties
        #region Private
        private ValidatableObject<decimal> estatura;
        private ValidatableObject<decimal> peso;
        private ValidatableObject<decimal> imc;
        private ValidatableObject<DateTime> fechanac;
        private ValidatableObject<string> confirmPassword;
        private ValidatableObject<string> email;
        private ValidatableObject<string> nombreCompleto;
        private ValidatableObject<string> user;
        private ValidatableObject<string> password;
        private ValidatableObject<char> sexo;
        #endregion
        #region Public
        public Guid IdUser { get; set; }
        public ValidatableObject<decimal> Imc
        {
            get { return imc; }
            set
            {
                imc = value;
                RaisePropertyChanged(() => Imc);
            }
        }
        private string _DiagImc;

        public string DiagImc
        {
            get { return _DiagImc; }
            set { 
                _DiagImc = value;
                RaisePropertyChanged(() => DiagImc);
            }
        }

        public ValidatableObject<decimal> Estatura
        {
            get { return estatura; }
            set
            {
                estatura = value;
                RaisePropertyChanged(() => Estatura);
            }
        }
        public ValidatableObject<decimal> Peso
        {
            get { return peso; }
            set
            {
                peso = value;
                RaisePropertyChanged(() => Peso);
            }
        }
        public ValidatableObject<DateTime> FechaNac
        {
            get { return fechanac; }
            set
            {
                fechanac = value;
                RaisePropertyChanged(() => FechaNac);
            }
        }
        public ValidatableObject<string> Usuario
        {
            get { return user; }
            set
            {
                user = value;
                RaisePropertyChanged(() => Usuario);
            }
        }
       
        public bool CanExecuteSave { get; set; }
        public Command SaveCommand { get; }

        public ValidatableObject<string> ConfirmPassword
        {
            get { return confirmPassword; }
            set
            {
                confirmPassword = value;
                RaisePropertyChanged(() => ConfirmPassword);
            }
        }
        public ValidatableObject<string> Email
        {
            get { return email; }
            set
            {
                email = value;
                RaisePropertyChanged(() => Email);
            }
        }
        public ValidatableObject<string> NombreCompleto
        {
            get { return nombreCompleto; }
            set
            {
                nombreCompleto = value;
                RaisePropertyChanged(() => NombreCompleto);
            }
        }
        public ValidatableObject<string> Password
        {
            get { return password; }
            set
            {
                password = value;
                RaisePropertyChanged(() => Password);
            }
        }
        public ValidatableObject<char> Sexo
        {
            get { return sexo; }
            set
            {
                sexo = value;
                RaisePropertyChanged(() => Sexo);
            }
        }
        #endregion
        #endregion
        #region Commands
        public ICommand ChangeGenderCommand => new Command<char>((x) => ChangeGender(x));
        public ICommand CalcImcCommand => new Command(async () => {
            estatura.Validate();
            peso.Validate();
            var x= await CalcImc(Peso.Value, Estatura.Value);
            Imc.Value = decimal.Round(x, 2, MidpointRounding.AwayFromZero);
            DiagImc = Imc.Value + " " + DiagnosticoImc();
        });
        public ICommand ValidateUserCommand => new Command(() => user.Validate());
        public ICommand ValidateFullNameCommand => new Command(() => nombreCompleto.Validate());
        public ICommand ValidateEmailCommand => new Command(() => email.Validate());
        public ICommand ValidatePasswordCommand => new Command(() => { password.Validate(); confirmPassword.Validate(); });
        public ICommand ValidateConfirmarPasswordCommand => new Command(() => { confirmPassword.Validate(); });
       
        #endregion
        #region Constructor
        public ProfileViewModel()
        {
            InitProperties();                      
            GetProperties();           
            CanExecuteSave = true;
            SaveCommand = new Command(async () => await SaveActionAsync(), () => CanExecuteSave);
        }
        #endregion
        #region Methods
        private void InitProperties()
        {
            user = new ValidatableObject<string>();
            nombreCompleto = new ValidatableObject<string>();
            email = new ValidatableObject<string>();
            password = new ValidatableObject<string>();
            confirmPassword = new ValidatableObject<string>();
            estatura = new ValidatableObject<decimal>();
            peso = new ValidatableObject<decimal>();
            imc = new ValidatableObject<decimal>();
            fechanac = new ValidatableObject<DateTime>();
            sexo = new ValidatableObject<char>();
            Password.Value = "1Abcdefg";
            ConfirmPassword.Value = "1Abcdefg";
        }
        private async Task<decimal> CalcImc(decimal peso, decimal altura)
        {
            try
            {
                if(peso!=0&&altura!=0 && peso> altura)
                {
                    decimal resultado;
                    resultado = peso / (altura * altura);
                    return resultado;
                }else
                {
                    return 0;

                }
                
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
                return 0;
            }

        }
        public string DiagnosticoImc()
        {
           
            if (Decimal.Compare(Imc.Value,(decimal)18.5)==-1)
            {
                return "Bajo peso";
            }
            else if(Decimal.Compare(Imc.Value, (decimal)18.5) == 1 && Decimal.Compare(Imc.Value, (decimal)25) == -1)
            {
                return "Peso normal";
            }
            else if (Decimal.Compare(Imc.Value, (decimal)25.0) == 1 && Decimal.Compare(Imc.Value, (decimal)30) == -1)
            {
                return "Sobrepeso";
            }
            else if (Decimal.Compare(Imc.Value, (decimal)30) == 1 && Decimal.Compare(Imc.Value, (decimal)34) == -1)
            {
                return "Obesidad I";
            }
            else if (Decimal.Compare(Imc.Value, (decimal)35.0) == 1 && Decimal.Compare(Imc.Value, (decimal)39) == -1)
            {
                return "Obesidad II";
            }
            else if (Decimal.Compare(Imc.Value, (decimal)40.0) == 1 && Decimal.Compare(Imc.Value, (decimal)50) == -1)
            {
                return "Obesidad III";
            }
            return "";

        }
        private void ChangeGender(char newGender)
        {
            sexo.Value = newGender;
            sexo.Validate();
        }
        private bool IsValid()
        {
            Usuario.Validate();
            peso.Validate();
            estatura.Validate();
            fechanac.Validate();
            nombreCompleto.Validate();
            email.Validate();
            password.Validate();
            confirmPassword.Validate();
            sexo.Validate();
            
            return Usuario.IsValid && nombreCompleto.IsValid && email.IsValid && password.IsValid && confirmPassword.IsValid && peso.IsValid && estatura.IsValid && fechanac.IsValid &&sexo.IsValid;
        }

        private void AddValidations()
        {
            nombreCompleto.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su nombre." });
            user.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su usuario" });
            email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su dirección de correo." });
            email.Validations.Add(new IsValidEmailRule { ValidationMessage = "Ingrese una cuenta de correo válida." });
            estatura.Validations.Add(new IsNotZeroRule { ValidationMessage = "Ingrese su estatura." });
            fechanac.Validations.Add(new FechaValidator { ValidationMessage = "Seleccione una fecha válida" });
            peso.Validations.Add(new IsNotZeroRule { ValidationMessage = "Ingrese su peso." });
            password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su contraseña." });
            password.Validations.Add(new IsValidPasswordRule { ValidationMessage = "Ingrese una contraseña válida.\r\nDebe contener al menos un número, una mayúscula, una minúscula.\r\nDebe contener una longitud de 8 a 16 caracteres." });
            confirmPassword.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Confirme su contraseña." });
            confirmPassword.Validations.Add(new IsEqualsThanRule<string> { ValidationMessage = "Las contraseñas no coinciden.", CompareTo = password });
            sexo.Validations.Add(new IsValidGenderRule { ValidationMessage = "Indique un género." });
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)SaveCommand))
                CanExecuteSave = value;
            command.ChangeCanExecute();
        }
        private async Task SaveActionAsync()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)SaveCommand);
                if (IsValid())
                {
                    Imc.Value = await CalcImc(Peso.Value, Estatura.Value);
                    if(Password.Value == "1Abcdefg")
                    {
                        Password.Value = MainViewModel.GetInstance().usuario.Contraseña;
                    }
                    User datos = new User
                    {
                        IdUsuario = IdUser,
                        NombreCompleto = NombreCompleto.Value,
                        Peso = Peso.Value,
                        Altura = Estatura.Value,
                        Correo = Email.Value,
                        Usuario = user.Value,                       
                        Telefono = "",
                        FechaNacimiento = FechaNac.Value,
                        Imc = decimal.Round(Imc.Value,2, MidpointRounding.AwayFromZero),
                        Contraseña = Password.Value,
                        Sexo = Sexo.Value
                    };                    
                    UserConsumer servicio = new UserConsumer(Network);
                    var result = await servicio.Actualizar(datos);
                    if (result.Equals(1))
                    {
                        await ShowAlertAsync("Exito", "Datos Actualizados");
                        MainViewModel.GetInstance().usuario = datos;
                        MessagingCenter.Send<ProfileViewModel>(this, "UpdateImg");
                       
                    }else if (result==2)
                    {
                        await ShowAlertAsync("Error", "El correo ya fue registrado");
                    }
                    else if(result==3)
                    {
                        await ShowAlertAsync("Error", "El usuario que ingresaste ya existe, escribe otro");
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "No se pudieron actualizar tus datos");
                    }
                    GetProperties();
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)SaveCommand);
            }
        }
        private async void GetProperties()
        {
            try
            {
                Loading.Show();
                IdUser = MainViewModel.GetInstance().usuario.IdUsuario;
                UserConsumer serv = new UserConsumer(Network);

                var result = await serv.Obtener(IdUser);
                if (result != null)
                {
                    NombreCompleto.Value = result.NombreCompleto;                   
                    Email.Value = result.Correo;
                    Usuario.Value = result.Usuario;
                    Password.Value = "1Abcdefg";                    
                    FechaNac.Value = result.FechaNacimiento;
                    Peso.Value = result.Peso;
                    Estatura.Value = result.Altura;
                    Imc.Value = result.Imc;
                    Sexo.Value = result.Sexo;
                    DiagImc = Imc.Value + " " + DiagnosticoImc();
                    /* if (!string.IsNullOrEmpty(result.Foto))
                     {
                         ImgUrl = result.Foto.StringBase64ToImageSource();
                         imgb64 = result.Foto;
                     }*/
                    AddValidations();
                }
                else
                {
                    await ShowAlertAsync("Error", "Ocurrio un problema al cargar tus datos");
                }

            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }
        #endregion
    }
}
