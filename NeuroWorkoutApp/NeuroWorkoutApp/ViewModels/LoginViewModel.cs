﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.Validations;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class LoginViewModel:BaseViewModel
    {
        #region Private Properties
        private ValidatableObject<string> email;
        private ValidatableObject<string> password;
        private bool rememberme;
        #endregion
        #region Public Properties
        public bool Rememberme
        {
            get { return rememberme; }
            set
            {
                rememberme = value;
                RaisePropertyChanged(() => Rememberme);
            }
        }
        public bool CanExecuteGoToLogin { get; set; }
        public bool CanExecuteGoToBack { get; set; }
        public ValidatableObject<string> Email
        {
            get { return email; }
            set { RaisePropertyChanged(() => Email); }
        }
        public ValidatableObject<string> Password
        {
            get { return password; }
            set { RaisePropertyChanged(() => Password); }
        }
        #endregion
        #region Commands
        public ICommand LoginCommand { get; private set; }
        public ICommand RegisterCommand { get; private set; }
        public ICommand ForgotAccountCommand { get; private set; }
        public ICommand BackCommand { get; private set; }
        public ICommand ValidateEmailCommand => new Command(() => email.Validate());
        public ICommand ValidatePasswordCommand => new Command(() => { password.Validate(); });
        #endregion
        #region Constructor
        public LoginViewModel()
        {
            InitProperties();
            AddValidations();
            CanExecuteGoToBack = true;
            CanExecuteGoToLogin = true;
            LoginCommand = new Command(LoginAction, canExecute: () => CanExecuteGoToLogin);           
            ForgotAccountCommand = new Command(ForgotAccount);
            MessagingCenter.Subscribe<App>((App)Application.Current, "AutoLogin", (sender) => {
                Email.Value = Preferences.Get("Correo","");
                Password.Value = Preferences.Get("Password", "");
                CanExecuteAction(false, (Command)LoginCommand);
                LoginAction();
            });
        }
        #endregion
        #region Methods
        public override async Task InitializeAsync(object parameters)
        {


            if (parameters !=null)
            {
                var valores = (object[])parameters;
                Email.Value = valores[0].ToString();
                Password.Value= valores[1].ToString();
                LoginAction();
            }
            await base.InitializeAsync(parameters);
        }
        private void InitProperties()
        {
            email = new ValidatableObject<string>();
            password = new ValidatableObject<string>();
           // Email.Value = "eacr77";
           // Password.Value = "7Eacrcru";
        }
        public async void LoginAction()
        {
            try
            {
                Loading.Show("Accediendo a NeuroWorkout");
                await Task.Delay(1000);
                CanExecuteAction(false, (Command)LoginCommand);
                if (IsValid()) {
                    User model = new User
                    {
                        Usuario = this.email.Value.Trim(),
                        Contraseña = this.password.Value,
                        IdTipoUsuario = 2
                    };
                    UserConsumer servicio = new UserConsumer(Network);
                    var result = await servicio.Ingresar(model);
                   
                    if (result.Error == 2)
                    {
                        await ShowAlertAsync("Error", "Contraseña Incorrecta");
                    }
                    else if (result.Error == 3)
                    {
                        await ShowAlertAsync("Error", "La cuenta no existe");
                    }
                    else 
                    {
                        MainViewModel.GetInstance().usuario = new User
                        {
                            IdUsuario = result.IdUsuario,
                            IdTipoUsuario = 2,
                            NombreCompleto = result.NombreCompleto,
                            FechaNacimiento = result.FechaNacimiento,
                            Peso = result.Peso,
                            Altura = result.Altura,
                            Imc = result.Imc,
                            Telefono = result.Telefono,
                            Correo = result.Correo,
                            Usuario = result.Usuario,
                            Contraseña= Password.Value
                        };
                        
                        if (Rememberme && !Preferences.ContainsKey(Email.Value))
                        {
                            Preferences.Set("Correo", Email.Value);
                            Preferences.Set("Password", Password.Value);
                        }
                            var navigationService = ServiceLocator.Resolve<INavigationService>();
                        await navigationService.RootNavigation(typeof(MasterViewModel));
                    }
                }                
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)LoginCommand);
            }
        }
       
        public async void ForgotAccount()
        {
            try
            {
                await Navigation.PushAsync(typeof(ForgotViewModel), true);
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)BackCommand))
                CanExecuteGoToBack = value;
            if (command.Equals((Command)LoginCommand))
                CanExecuteGoToLogin = value;
            command.ChangeCanExecute();
        }
        private bool IsValid()
        {
            email.Validate();
            password.Validate();
            return email.IsValid && password.IsValid;
        }
        private void AddValidations()
        {
            email.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su usuario" });
            //email.Validations.Add(new IsValidEmailRule { ValidationMessage = "Ingrese una cuenta de correo válida." });
            password.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Ingrese su contraseña." });
        }
        #endregion
    }
}
