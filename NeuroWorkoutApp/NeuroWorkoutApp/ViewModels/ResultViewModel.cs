﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class ResultViewModel:BaseViewModel
    {
        #region Properties
        public bool CanExecTrain { get; set; }
        public TestModel Test { get; set; }
        private string _txtEstres;

        public string TxtEstres
        {
            get { return _txtEstres; }
            set { 
                _txtEstres = value;
                RaisePropertyChanged(() => TxtEstres);
            }
        }
        private string _txtAnsiedad;

        public string TxtAnsiedad
        {
            get { return _txtAnsiedad; }
            set { _txtAnsiedad = value;
                RaisePropertyChanged(() => TxtAnsiedad);
            }
        }
        private string _txtDepresion;

        public string TxtDepresion
        {
            get { return _txtDepresion; }
            set { _txtDepresion = value;
                RaisePropertyChanged(() => TxtDepresion);
            }
        }
       
        private int _PorcAnsiedad;

        public int PorcAnsiedad
        {
            get { return _PorcAnsiedad; }
            set { _PorcAnsiedad = value;
                RaisePropertyChanged(() => PorcAnsiedad);
            }
        }
        private int _procEstres;

        public int PorcEstres
        {
            get { return _procEstres; }
            set { _procEstres = value;
                RaisePropertyChanged(() => PorcEstres);
            }
        }
        private int _PorcDepresion;

        public int PorcDepresion
        {
            get { return _PorcDepresion; }
            set { _PorcDepresion = value;
                RaisePropertyChanged(() => PorcDepresion);
            }
        }


        #endregion
        #region Commands
        public ICommand TrainCommand { get; set; }
        #endregion
        #region Constuctor
        public ResultViewModel()
        {
            CanExecTrain = true;
            TrainCommand = new Command(async () => await IniciarAction(), () => CanExecTrain);
        }
        #endregion
        #region Methods
        private int TipoWod()
        {
            Random rnd = new Random();
            if (Test.NivelEstres > Test.NivelAnsiedad && Test.NivelEstres > Test.NivelDepresion)
            {
                return 1;
            }
            else if (Test.NivelDepresion > Test.NivelEstres && Test.NivelDepresion > Test.NivelAnsiedad)
            {
                return 2;
            }
            else if(Test.NivelAnsiedad > Test.NivelEstres && Test.NivelAnsiedad > Test.NivelDepresion)
            {
                return 3;
            }
            else if (Test.PuntajeEstres>Test.PuntajeAnsiedad && Test.PuntajeEstres>Test.PuntajeDepresion)
            {
                return 1;
            }else if(Test.PuntajeDepresion>Test.PuntajeEstres && Test.PuntajeDepresion>Test.PuntajeAnsiedad)
            {
                return 2;
            }else if (Test.PuntajeAnsiedad>Test.PuntajeEstres && Test.PuntajeAnsiedad>Test.PuntajeDepresion)
            {
                return 3;
            }else if (Test.PuntajeAnsiedad == Test.PuntajeDepresion && Test.PuntajeDepresion == Test.PuntajeEstres)
            {

                var x = rnd.Next(1, 3);
                return x;
            }
            else if (Test.PuntajeEstres == Test.PuntajeAnsiedad)
            {
                if (Test.NivelEstres > Test.NivelAnsiedad )
                {
                    return 1;
                }
                else
                {
                    return 3;
                }
               
            }
            else if (Test.PuntajeDepresion == Test.PuntajeAnsiedad)
            {
                if (Test.NivelDepresion > Test.NivelAnsiedad)
                {
                    return 2;
                }
                else
                {
                    return 3;
                }
            }
            else if (Test.PuntajeDepresion == Test.PuntajeEstres)
            {
                if (Test.NivelDepresion > Test.NivelEstres)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
            return rnd.Next(1, 3);
        }
        private async Task IniciarAction()
        {
            try
            {
                CanExecuteAction(false, (Command)TrainCommand);
                if(Test.EsInicial)
                {
                    var t = TipoWod();
                    await Navigation.MasterPushAsync(typeof(WodViewModel),t,true);
                }                                
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)TrainCommand);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)TrainCommand))
            {
                CanExecTrain = value;
            }
            command.ChangeCanExecute();
        }
        public void CalcularPorcentajes()
        {
            try
            {
                int totalAnsiedad = 21;
                int totalEstres = 21;
                int totalDepresion = 21;
                PorcAnsiedad = (Test.PuntajeAnsiedad * 100) / totalAnsiedad;
                PorcEstres = (Test.PuntajeEstres * 100) / totalEstres;
                PorcDepresion = (Test.PuntajeDepresion * 100) / totalDepresion;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public override async Task InitializeAsync(object parameters)
        {


            if (parameters is TestModel model )
            {
                Test = (TestModel)parameters;
                TxtAnsiedad = Test.ResultadoAnsiedad;
                TxtDepresion = Test.ResultadoDepresion;
                TxtEstres = Test.ResultadoEstres;
                CalcularPorcentajes();

            }
            await base.InitializeAsync(parameters);
        }
        #endregion
    }
}
