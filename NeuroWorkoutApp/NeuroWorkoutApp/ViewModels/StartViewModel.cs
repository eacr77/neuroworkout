﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class StartViewModel : BaseViewModel
    {
        #region Properties
        public bool CanExecuteGoToLogin { get; set; }
        public bool CanExecuteGoToRegister { get; set; }
        public CarouselData MyData { get; set; }
        ObservableCollection<CarouselData> _myItemsSource;
        public ObservableCollection<CarouselData> MyDataSource
        {

            get
            {
                return _myItemsSource;
            }
            set
            {
                _myItemsSource = value;
                OnPropertyChanged();
            }
        }
        #endregion
        #region Commands
        public ICommand LoginCommand { get; private set; }
        public ICommand RegisterCommand { get; private set; }
        #endregion
        #region Constructor
        public StartViewModel()
        {
            CanExecuteGoToLogin = true;
            CanExecuteGoToRegister = true;
            LoginCommand = new Command(LoginAction, canExecute: () => CanExecuteGoToLogin);
            RegisterCommand = new Command(RegisterAction, canExecute: () => CanExecuteGoToRegister);
            MyDataSource = new ObservableCollection<CarouselData> {

                new CarouselData(){
                    Title="Bienvenido",
                    Detail="Registrate para entrenar"
                },
                new CarouselData(){
                    Title="Vive la experiencia",
                    Detail="Functional NeuroTraining"
                },
                new CarouselData(){
                    Title="Conoce",
                    Detail="NeuroWorkout"
                },
            };
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)RegisterCommand))
                CanExecuteGoToRegister = value;
            if (command.Equals((Command)LoginCommand))
                CanExecuteGoToLogin = value;
            command.ChangeCanExecute();
        }
        private async void RegisterAction()
        {
            try
            {
                CanExecuteAction(false, (Command)RegisterCommand);                
                await Navigation.PushAsync(typeof(RegisterViewModel), true);
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)RegisterCommand);
            }
        }

        private async void LoginAction()
        {
            try
            {
                CanExecuteAction(false, (Command)LoginCommand);                
                await Navigation.PushAsync(typeof(LoginViewModel), true);
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                CanExecuteAction(true, (Command)LoginCommand);
            }
        }
        #endregion
    }
}
