﻿using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services.Api;
using NeuroWorkoutApp.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class RoutineViewModel : BaseViewModel
    {
        #region Properties
        public bool CanExecIniciar { get; set; }
        public Guid IdWod { get; set; }
        public int TipoWod { get; set; }
        public int TotalRound { get; set; }
        private string _titulo;

        public string Titulo
        {
            get { return _titulo; }
            set { 
                _titulo = value;
                RaisePropertyChanged(() => Titulo);
            }
        }
        private List<EjercicioWodModel> _Ejercicios;

        public List<EjercicioWodModel> Ejercicios
        {
            get { return _Ejercicios; }
            set
            {
                _Ejercicios = value;
                RaisePropertyChanged(() => Ejercicios);
            }
        }
        #endregion
        #region Commands
        public ICommand IniciarCommand { get; set; }
        public ICommand SelectedExerciseCommand => new Command<EjercicioWodModel>(execute: async (item) => {
            EjercicioModel ejer = new EjercicioModel();
            ejer.IdEjercicio = item.IdEjercicio;
            ejer.Nombre = item.Nombre;            
            await NavegarPagina(ejer);
        });
        #endregion
        #region Constructor
        public RoutineViewModel()
        {
            IniciarCommand = new Command(async () => await IniciarAction(), () => CanExecIniciar);           
           
        }
        #endregion
        #region Methods
        public async Task LoadEjercicios()
        {
            try
            {
                Loading.Show();
                TrainingConsumer services = new TrainingConsumer(Network);
                var result = await services.GetEjercicioWod(IdWod, TipoWod);         
                Ejercicios = new List<EjercicioWodModel>();
                if (result != null)
                {
                    if (result.Count != 0)
                    {
                        Ejercicios = result;
                    }
                    else
                    {
                        await ShowAlertAsync("Error", "Ocurrio un problema al cargar tus datos");
                    }
                }
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                Loading.Hide();
            }
        }
        public override async Task InitializeAsync(object parameters)
        {
            try
            {
                if (parameters is WodCompleto)
                {
                    Loading.Show();
                    var wod = (WodCompleto)parameters;
                    TipoWod = wod.TipoWod;
                    TotalRound = wod.TipoWod==3?wod.TotalRound:1;
                    switch (TipoWod)
                    {
                        case 1:
                            {
                                Titulo = "Estiramiento";
                                IdWod = wod.IdEstiramiento;
                            }                            
                             
                            break;
                        case 2:
                            {
                                Titulo = "Calentamiento";
                                IdWod = wod.IdCalentamiento;
                            }
                            
                            break;
                        case 3:
                            {
                                Titulo = "WOD";
                                IdWod = wod.Id;
                            }                            
                            break;
                        case 4:
                            {
                                Titulo = "Relajación";
                                IdWod = wod.IdEstiramiento2;
                            }                            
                            break;
                    }
                   await LoadEjercicios();
                    CanExecuteAction(true, (Command)IniciarCommand);

                }
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
            }
        }

        private async Task NavegarPagina(EjercicioModel item)
        {
            
           await Navigation.MasterPushAsync(typeof(ExerDetViewModel),item, true);
        }
        private async Task IniciarAction()
        {
            try
            {
                CanExecuteAction(false, (Command)IniciarCommand);
                object[] it= { Ejercicios, TipoWod,TotalRound };
                await Navigation.MasterPushAsync(typeof(ExerTrainingViewModel), it, true);               
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)IniciarCommand);
            }
        }
        private void CanExecuteAction(bool value, Command command)
        {
            if (command.Equals((Command)IniciarCommand))
            {
                CanExecIniciar = value;
            }
            command.ChangeCanExecute();
        }
        #endregion
    }
}
