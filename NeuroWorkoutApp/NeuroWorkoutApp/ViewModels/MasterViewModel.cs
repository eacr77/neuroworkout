﻿using NeuroWorkoutApp.Extensions;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using NeuroWorkoutApp.Services;
using NeuroWorkoutApp.ViewModels.Base;
using NeuroWorkoutApp.ViewModels.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace NeuroWorkoutApp.ViewModels
{
   public class MasterViewModel:BaseViewModel
    {
        #region Properties
        public bool CanExecTrain { get; set; }
        public bool CanExecuteGoToLogout { get; set; }
        public bool CanExecuteGoToProfile { get; set; }
        private ObservableCollection<MenuItemViewModel> _menus;

        public ObservableCollection<MenuItemViewModel> Menus
        {
            get { return _menus; }
            set
            {
                _menus = value;
                OnPropertyChanged();
            }
        }
        private ImageSource _PicUrl;

        public ImageSource PictureUrl
        {
            get { return _PicUrl; }
            set
            {
                _PicUrl = value;
                OnPropertyChanged();
            }
        }
        private string _Nombre;

        public string NombreCompleto
        {
            get { return _Nombre; }
            set
            {
                _Nombre = value;
                OnPropertyChanged();
            }
        }
        #endregion
       
        #region Constructor
        public MasterViewModel()
        {
            CanExecuteGoToProfile = true;
            CanExecuteGoToLogout = true;
            CanExecTrain = true;
            ProfileCommand = new Command(ProfileAction, canExecute: () => CanExecuteGoToProfile);
            LogoutCommand = new Command(LogoutAction, canExecute: () => CanExecuteGoToLogout);
            TrainCommand = new Command(async () => await IniciarAction(), () => CanExecTrain);
            LoadMenus();
            LoadProfile();
            MessagingCenter.Subscribe<ProfileViewModel>(this, "UpdateImg", (sender) => {
                LoadProfile();
            });
        }

       
        #endregion
        #region Commands
        public ICommand ProfileCommand { get; private set; }
        public ICommand LogoutCommand { get; private set; }
        public ICommand TrainCommand { get; set; }
        #endregion
        #region Methods
        private async Task IniciarAction()
        {
            try
            {
                CanExecuteAction(false, (Command)TrainCommand);
                await Navigation.MasterPushAsync(typeof(TrainingViewModel), true);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.ToString());
            }
            finally
            {
                CanExecuteAction(true, (Command)TrainCommand);
            }
        }
        private async void LoadProfile()
        {
            try
            {
                if (string.IsNullOrEmpty(MainViewModel.GetInstance().usuario.Foto))
                {
                    PictureUrl = "face";
                }
                else
                {
                    PictureUrl = MainViewModel.GetInstance().usuario.Foto.StringBase64ToImageSource();
                }
                NombreCompleto = MainViewModel.GetInstance().usuario.NombreCompleto;                
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", "Ocurrio un problema,"+ ex.Message);
            }
        }
        private async void ProfileAction()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)ProfileCommand);
                await Task.Delay(1000);
                var navigationService = ServiceLocator.Resolve<INavigationService>();
                MasterDetailPage masterDetailRootPage = (MasterDetailPage)Application.Current.MainPage;
                masterDetailRootPage.IsPresented = false;
                await navigationService.MasterPushAsync(typeof(ProfileViewModel), true);
            }
            catch (Exception ex)
            {

                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)ProfileCommand);
            }
        }
        private async void LogoutAction()
        {
            try
            {
                Loading.Show();
                CanExecuteAction(false, (Command)LogoutCommand);
                Preferences.Clear();
                var navigationService = ServiceLocator.Resolve<INavigationService>();
                await navigationService.CreateNavigation(typeof(StartViewModel), true);
            }
            catch (Exception ex)
            {
                await ShowAlertAsync("Error", ex.Message);
            }
            finally
            {
                Loading.Hide();
                CanExecuteAction(true, (Command)LogoutCommand);
            }
        }
        public void LoadMenus()
        {
            var menus = new List<MenuApp>
            {
                new MenuApp
                {
                    Icon="gym",
                    Title="Inicio",
                    ViewModelName="HomeViewModel"
                },
                new MenuApp
                {
                    Icon="abs",
                    Title="Entrenamiento",
                    ViewModelName="TrainingViewModel"
                },
                new MenuApp
                {
                    Icon="tenis",
                    Title="Ejercicios",
                    ViewModelName="ExerciseViewModel"
                },
                new MenuApp
                {
                    Icon="repo",
                    Title="Repositorio",
                    ViewModelName="RepositoryViewModel"
                }

            };
            this.Menus = new ObservableCollection<MenuItemViewModel>(menus.Select(m => new MenuItemViewModel
            {
                menu = m
            }).ToList());
        }
        private void CanExecuteAction(bool value, Command command)
        {
            
            if (command.Equals((Command)LogoutCommand))
                CanExecuteGoToLogout = value;
            if (command.Equals((Command)ProfileCommand))
                CanExecuteGoToProfile = value;
            if (command.Equals((Command)TrainCommand))
            {
                CanExecTrain = value;
            }
            command.ChangeCanExecute();
        }
        #endregion
    }
}
