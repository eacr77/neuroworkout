﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Helpers
{
    public static class Messages
    {
        #region ErrorMessages
        public const string ErrorTitle = "Error";
        public const string SuccessTitle = "";

        public const string GeneralErrorMessage = "Lo sentimos. Algo salió mal.";
        public const string ExpiredTokenError = "Lo sentimos, tu sesión ha expirado.";

        public const string NoInternetTitleError = "Error de conexión a Internet.";
        public const string NoInternetError = "Se requiere conexión a Internet para ésta solicitud.";

        public const string UnauthorizedError = "No autorizado";
        public const string NotAuthenticatedError = "Debe estar autenticado para realizar ésta acción.";

        public const string NoResultsFoundError = "Sin resultados";
        #endregion

        #region LoadingMessages
        public const string PleaseWait = "Espere un momento...";
        #endregion

        #region DialogMessages
        public const string Ok = "OK";
        public const string No = "No";
        public const string Yes = "Si";
        public const string Cancel = "Cancelar";
        public const string DoYouWantDeleteRecord = "¿Desea eliminar el registro?";
        #endregion

        #region ConfirmMessages
        public const string ConfirmTitle = "Confirme la acción a realizar";
        #endregion

        #region Time
        /// <summary>
        /// Tiempo del toast en pantalla en milisegundos
        /// </summary>
        public const int TimeToast = 2000;
        #endregion

        #region Colors
        public const string GreenColor = " #00E676";
        public const string WhiteColor = " #00E676";
        #endregion
    }
}
