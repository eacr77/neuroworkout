﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Helpers.Formatter
{
   public class SignedNumericFormatter : IFormatProvider, ICustomFormatter
    {

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            else
                return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            // Check whether this is an appropriate callback             
            if (!this.Equals(formatProvider))
                return null;

            // Set default format specifier             
            if (string.IsNullOrEmpty(format))
                format = "N";

            decimal.TryParse(arg.ToString(), out decimal value);

            if (format == "N")
            {
                if (value > 0)
                    return string.Format("+ {0:N0}", value);
                else
                    return string.Format("- {0:N0}", Math.Abs(value));
            }
            else
            {
                throw new FormatException(string.Format("The {0} format specifier is invalid.", format));
            }
            //return string.Empty;
        }
    }
}
