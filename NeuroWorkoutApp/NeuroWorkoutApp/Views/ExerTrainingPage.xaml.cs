﻿using NeuroWorkoutApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NeuroWorkoutApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExerTrainingPage : ContentPage
    {
        public ExerTrainingPage()
        {
            InitializeComponent();
        }
        protected override bool OnBackButtonPressed()
        {
            var vm = (ExerTrainingViewModel)BindingContext;
            if (vm.BackCommand.CanExecute(null))  // You can add parameters if any
            {
                vm.BackCommand.Execute(null); // You can add parameters if any
                return true;
            }
            return false;
        }
    }
}