﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Services;
using NeuroWorkoutApp.ViewModels;
using NeuroWorkoutApp.Views;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NeuroWorkoutApp
{
    public partial class App : Application
    {
        public static NavigationPage Navigator { get; internal set; }
        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzEzODkwQDMxMzgyZTMyMmUzMFdtQStFbVVqZjlDNS9pK2d3djlHSGN4c05GMFFmL3pJYUVrK3VoT0srVWs9");
            InitializeComponent();           
           InitializeNavigation();
        }
        private void InitializeNavigation()
        {
            var navigationService = ServiceLocator.Resolve<INavigationService>();
            if(Preferences.ContainsKey("Correo"))
            {
                MessagingCenter.Send((App)Current, "AutoLogin");
                var user = Preferences.Get("Correo", "");
                var password = Preferences.Get("Password", "");
                object[] valores = { user, password };
                navigationService.CreateNavigation<LoginViewModel>(valores);
            }
            else
            {
                navigationService.CreateNavigation<StartViewModel>();
            }            
        }
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
