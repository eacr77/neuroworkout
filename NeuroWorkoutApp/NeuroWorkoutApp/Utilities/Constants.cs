﻿using System;


namespace NeuroWorkoutApp.Utilities
{
   public class Constants
    {
        #region HttpClient
        public const int MaxResponseContentBufferSize = 999999;
        #endregion

        #region WS URI
        public static Uri BASE_URI = new Uri("https://neuroapp.azurewebsites.net/");
        public static string DEBUG_BASE_URI = "";
        public static string PRODUCTION_BASE_URI = "https://neuroapp.azurewebsites.net/";
        #endregion
    }
}
