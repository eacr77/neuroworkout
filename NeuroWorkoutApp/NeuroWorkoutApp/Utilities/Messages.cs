﻿
namespace NeuroWorkoutApp.Utilities
{
   public static class Messages
    {

        #region ErrorMessages
        public const string ErrorTitle = "Error";

        public const string GeneralErrorMessage = "Lo sentimos. Algo salió mal.";
        public const string ExpiredTokenError = "Lo sentimos, tu sesión ha expirado.";

        public const string NoInternetTitleError = "Error de conexión a Internet.";
        public const string NoInternetError = "Se requiere conexión a Internet para ésta solicitud.";

        public const string UnauthorizedError = "No autorizado";
        public const string NotAuthenticatedError = "Debe estar autenticado para realizar ésta acción.";

        public const string NoResultsFoundError = "Sin resultados";
        #endregion

        #region LoadingMessages
        public const string PleaseWait = "Espere un momento...";
        #endregion

        #region DialogMessages
        public const string Ok = "OK";
        public const string No = "No";
        public const string Yes = "Si";
        public const string Cancel = "Cancelar";
        #endregion
    }
}
