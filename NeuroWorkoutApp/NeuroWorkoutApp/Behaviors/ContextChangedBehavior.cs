﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Behaviors
{
    public class ContextChangedBehavior : Behavior<Xamarin.Forms.View>
    {
        private Xamarin.Forms.View view;

        protected override void OnAttachedTo(BindableObject bindable)
        {
            base.OnAttachedTo(bindable);
            view = bindable as Xamarin.Forms.View;
            view.BindingContextChanged += OnBindingContextChanged;
        }

        protected override void OnDetachingFrom(BindableObject bindable)
        {
            base.OnDetachingFrom(bindable);
            view.BindingContextChanged -= OnBindingContextChanged;

        }

        void OnBindingContextChanged(object sender, EventArgs e)
        {
            var x = view.BindingContext;
        }
    }
}
