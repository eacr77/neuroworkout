﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Behaviors
{
   public class ToUpperBehavior : Behavior<Xamarin.Forms.View>
    {
        private Xamarin.Forms.View view;

        public string PropertyName { get; set; }

        protected override void OnAttachedTo(BindableObject bindable)
        {
            base.OnAttachedTo(bindable);
            view = bindable as Xamarin.Forms.View;
            view.Unfocused += OnUnFocused;
        }

        protected override void OnDetachingFrom(BindableObject bindable)
        {
            base.OnDetachingFrom(bindable);
            view.Unfocused -= OnUnFocused;
        }

        void OnUnFocused(object sender, FocusEventArgs e)
        {
            object value = view.GetType().GetProperty(PropertyName).GetValue(view);
            if (value != null)
                view.GetType().GetProperty(PropertyName).SetValue(view, value.ToString().ToUpper());
        }
    }
}
