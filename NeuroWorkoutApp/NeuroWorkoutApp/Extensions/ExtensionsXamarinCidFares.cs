﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;


namespace NeuroWorkoutApp.Extensions
{
    public static class ExtensionsXamarinCidFares
    {
        public static Xamarin.Forms.ImageSource StringBase64ToImageSource(this string base64)
        {
            byte[] Base64Stream = Convert.FromBase64String(base64);
            return ImageSource.FromStream(() => new MemoryStream(Base64Stream));
        }

        public static double Clamp(this double self, double min, double max)
        {
            return Math.Min(max, Math.Max(self, min));
        }
    }
}
