﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NeuroWorkoutApp.Extensions
{
    public static class ImgStreamToB64
    {
        public static string ConvertToBase64(this Stream stream)
        {
            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                bytes = memoryStream.ToArray();
            }

            string base64 = Convert.ToBase64String(bytes);
            return base64;
        }
    }
}
