﻿using System;
using System.Reflection;
namespace NeuroWorkoutApp.Extensions
{
    public static class TypeExtension
    {
        public static T Clone<T>(this T S)
        {
            T newObj = Activator.CreateInstance<T>();

            foreach (PropertyInfo i in newObj.GetType().GetProperties())
            {

                //"EntitySet" is specific to link and this conditional logic is optional/can be ignored
                //if (i.CanWrite && i.PropertyType.Name.Contains("EntitySet") == false)
                //{
                object value = S.GetType().GetProperty(i.Name).GetValue(S, null);
                i.SetValue(newObj, value, null);
                //}
            }

            return newObj;
        }
    }
}
