﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using NeuroWorkoutApp.Helpers;
using System.Text;

namespace NeuroWorkoutApp.Models.Base
{
   public class BaseEntity : ObservableCollection
    {
        [JsonProperty(PropertyName = "Status")]
        public int Status { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }
    }
}
