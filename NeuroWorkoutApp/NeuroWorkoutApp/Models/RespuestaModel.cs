﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class RespuestaModel
    {
       
        public Guid IdPreguntaTest { get; set; }
     
        public int Respuesta { get; set; }
    }
}
