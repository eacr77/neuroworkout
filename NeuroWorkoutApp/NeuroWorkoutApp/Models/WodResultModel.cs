﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class WodResultModel
    {
        public Guid IdWod { get; set; }
        public Guid IdCliente { get; set; }
        public int Calificacion { get; set; }
        public string Restrospectiva { get; set; }
    }
}
