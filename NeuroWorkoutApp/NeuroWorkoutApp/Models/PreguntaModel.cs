﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class PreguntaModel
    {

        public Guid IdTest { get; set; }
        public Guid IdPreguntaTest { get; set; }
        public string Pregunta { get; set; }
        public int Orden { get; set; }
        public int Valor { get; set; }
    }
}
