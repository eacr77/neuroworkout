﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
  public class EjercicioPreviewModel
    {
        public Guid IdEjercicio { get; set; }
        public int IdTipoEjercicio { get; set; }
        public string TipoEjercicio { get; set; }
        public string NombreEjercicio { get; set; }
        public string Descripcion { get; set; }
        public string UrlMedia { get; set; }
        public bool Descanso { get; set; }
    }
}
