﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models.System
{
   public class PaginatorModel
    {
        public int Page { get; set; }
        public int ItemsXPage { get; set; }
        public string Search { get; set; }
    }
}
