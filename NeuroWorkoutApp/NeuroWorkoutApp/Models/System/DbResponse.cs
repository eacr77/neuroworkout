﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
namespace NeuroWorkoutApp.Models.System
{
   public class DbResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "errorMessage")]
        public string ErrorMessage { get; set; }
    }
}
