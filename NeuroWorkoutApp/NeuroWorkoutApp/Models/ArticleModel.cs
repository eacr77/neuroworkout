﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class ArticleModel
    {
        public string Nombre { get; set; }
        public string TipoA { get; set; }
        public string Fecha { get; set; }
        public string UrlArchivo { get; set; }
        public string Foto { get; set; }
    }
}
