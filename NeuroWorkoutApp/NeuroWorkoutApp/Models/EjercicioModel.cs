﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class EjercicioModel
    {
        public Guid IdEjercicio { get; set; }
        public int IdTipoEjercicio { get; set; }
        public string TipoEjercicio { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string UrlMedia { get; set; }
        public bool Descanso { get; set; }
        public bool EsVideo { get; set; }
    }
}
