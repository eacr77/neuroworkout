﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class WodEjercicio
    {
        public string IdEjercicio { get; set; }
        public int Reps { get; set; }
        public string Nombre { get; set; }
    }
}
