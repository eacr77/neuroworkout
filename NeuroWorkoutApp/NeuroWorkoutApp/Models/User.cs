﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
  public class User
    {
        public Guid IdUsuario { get; set; }
        public int IdTipoUsuario { get; set; }
        public string NombreCompleto { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public decimal Peso { get; set; }
        public decimal Altura { get; set; }
        public decimal Imc { get; set; }
        public char Sexo { get; set; }
        public string Foto { get; set; }
        public int Error { get; set; }
    }
}
