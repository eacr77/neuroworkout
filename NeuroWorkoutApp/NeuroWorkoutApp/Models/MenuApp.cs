﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
   public class MenuApp
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string ViewModelName { get; set; }
    }
}
