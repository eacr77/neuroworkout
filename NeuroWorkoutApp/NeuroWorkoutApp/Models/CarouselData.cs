﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Models
{
  public class CarouselData
    {
        public string Title { get; set; }
        public string Detail { get; set; }
    }
}
