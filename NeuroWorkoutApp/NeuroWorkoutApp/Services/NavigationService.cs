﻿using System;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.ViewModels.Base;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Services
{
   public class NavigationService : INavigationService
    {

        #region Create Page Instance

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewNameTemp = viewModelType.FullName.Replace(".ViewModels", ".Views");
            var viewName = viewNameTemp.Replace("ViewModel", "Page");
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        public Page CreateAndBindPage(Type viewModelType, object parameter)
        {
            try
            {
                Type pageType = GetPageTypeForViewModel(viewModelType);
                if (pageType == null)
                {
                    throw new Exception($"Cannot locate page type for {viewModelType}");
                }
                Page page = Activator.CreateInstance(pageType) as Page;
                var viewModel = ServiceLocator.Resolve(viewModelType);
                if (viewModel != null)
                    page.BindingContext = viewModel;
                if (parameter != null) (viewModel as BaseViewModel).InitializeAsync(parameter);
                return page;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        #endregion

        #region Navigation

        public async Task PopToRootAsync(bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PopToRootAsync(animated);
        }

        public async Task RootNavigation(Type viewModel, object parameter = null)
        {
            Application.Current.MainPage = CreateAndBindPage(viewModel, parameter);
            await Task.FromResult(true);
        }

        public async Task RootNavigation<TViewModel>(object parameter = null) where TViewModel : BaseViewModel
        {
            Application.Current.MainPage = CreateAndBindPage(typeof(TViewModel), parameter);
            await Task.FromResult(true);
        }

        public async Task PopAsync(bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PopAsync(animated);
        }
        public async Task MasterPopToRootAsync(bool animated = true)
        {
            await App.Navigator.PopToRootAsync(animated);
        }
        public async Task MasterPopAsync(bool animated = true)
        {
            await App.Navigator.PopAsync(animated);
        }

        public async Task PopModalAsync(bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PopModalAsync(animated);
        }

        public async Task PushAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel
        {
            await Application.Current.MainPage.Navigation.PushAsync(CreateAndBindPage(typeof(TViewModel), null), animated);
        }

        public async Task PushAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel
        {
            await Application.Current.MainPage.Navigation.PushAsync(CreateAndBindPage(typeof(TViewModel), parameter), animated);
        }

        public async Task PushAsync(Type viewModel, bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PushAsync(CreateAndBindPage(viewModel, null), animated);
        }

        public async Task PushAsync(Type viewModel, object parameter = null, bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PushAsync(CreateAndBindPage(viewModel, parameter), animated);
        }
        #region Navigation From a MasterPage
        public async Task MasterPushAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel
        {
            await App.Navigator.PushAsync(CreateAndBindPage(typeof(TViewModel), null), animated);
        }

        public async Task MasterPushAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel
        {
            await App.Navigator.PushAsync(CreateAndBindPage(typeof(TViewModel), parameter), animated);
        }

        public async Task MasterPushAsync(Type viewModel, bool animated = true)
        {
            await App.Navigator.PushAsync(CreateAndBindPage(viewModel, null), animated);
        }

        public async Task MasterPushAsync(Type viewModel, object parameter = null, bool animated = true)
        {
            await App.Navigator.PushAsync(CreateAndBindPage(viewModel, parameter), animated);
        }
        #endregion

        public async Task PushModalAsync(Type viewModel, bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(CreateAndBindPage(viewModel, null), animated);
        }

        public async Task PushModalAsync(Page view, bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(view, animated);
        }

        public async Task PushModalAsync(Type viewModel, object parameter = null, bool animated = true)
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(CreateAndBindPage(viewModel, parameter), animated);
        }

        public async Task PushModalAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(CreateAndBindPage(typeof(TViewModel), null), animated);
        }

        public async Task PushModalAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(CreateAndBindPage(typeof(TViewModel), parameter), animated);
        }

        public async Task CreateNavigation(Type viewModel, object parameter = null)
        {
            App.Current.MainPage = new NavigationPage(CreateAndBindPage(viewModel, parameter));
            await Task.FromResult(true);
        }

        public async Task CreateNavigation<TViewModel>(object parameter = null) where TViewModel : BaseViewModel
        {
            App.Current.MainPage = new NavigationPage(CreateAndBindPage(typeof(TViewModel), parameter));
            await Task.FromResult(true);
        }

        #endregion

        #region Navigation Additional


        public Task RemoveLastFromBackStackAsync()
        {
            //var mainPage = Application.Current.MainPage as NavigationPage;
            var mainPage = App.Navigator;
            if (mainPage != null)
            {
                var page = mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2];
                mainPage.Navigation.RemovePage(page);
            }

            return Task.FromResult(true);
        }

        public Task RemoveBackStackAsync()
        {
            var mainPage = Application.Current.MainPage as NavigationPage;

            if (mainPage != null)
            {
                for (int i = 0; i < mainPage.Navigation.NavigationStack.Count - 1; i++)
                {
                    var page = mainPage.Navigation.NavigationStack[i];
                    mainPage.Navigation.RemovePage(page);
                }
            }

            return Task.FromResult(true);
        }


        #endregion
    }
}
