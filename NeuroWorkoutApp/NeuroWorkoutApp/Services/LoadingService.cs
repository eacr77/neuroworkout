﻿using Acr.UserDialogs;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Interfaces;
namespace NeuroWorkoutApp.Services
{
   public class LoadingService : ILoadingService
    {
        public void Hide()
        {
            UserDialogs.Instance.HideLoading();
        }

        public void Show(string message = Messages.PleaseWait, MaskType mask = MaskType.Black)
        {
            UserDialogs.Instance.ShowLoading(message, mask);
        }
    }
}
