﻿using System;
using System.Collections.Generic;
using System.Text;
using TinyIoC;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.ViewModels;
using NeuroWorkoutApp.Services.RequestProvider;

namespace NeuroWorkoutApp.Services
{
    public static class ServiceLocator
    {
        private static TinyIoCContainer _container;
        public static bool UseMockService { get; set; }
        static ServiceLocator()
        {
            _container = new TinyIoCContainer();
            _container.Register<App>();
            _container.Register<MainViewModel>();
            _container.Register<StartViewModel>();
            _container.Register<ForgotViewModel>();
            _container.Register<ProfileViewModel>();
            _container.Register<RegisterViewModel>();
            _container.Register<MasterViewModel>();
            _container.Register<LoginViewModel>();
            _container.Register<SurveyViewModel>();
            _container.Register<ResultViewModel>();
            _container.Register<WodViewModel>();
            _container.Register<RoutineViewModel>();
            _container.Register<ExerDetViewModel>();
            _container.Register<ExerTrainingViewModel>();
            //servicios
            _container.Register<IDialogService, DialogService>();
            _container.Register<INetworkService, NetworkService>();
            _container.Register<ILoadingService, LoadingService>();
            _container.Register<INavigationService, NavigationService>();
            _container.Register<IRequestProvider, RequestProviderService>();
        }
        public static void UpdateDependencies(bool useMockServices)
        {
            // Change injected dependencies
            if (useMockServices)
            {                
                UseMockService = true;
            }
            else
            {                
                UseMockService = false;
            }
        }
        public static void RegisterSingleton<TInterface, T>() where TInterface : class where T : class, TInterface
        {
            _container.Register<TInterface, T>().AsSingleton();
        }

        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }

        public static object Resolve(Type type) => _container.Resolve(type);
    }
}
