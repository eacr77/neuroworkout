﻿using NeuroWorkoutApp.Extensions;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models.Base;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.Services.Base
{
   public class BaseConsumer
    {
        protected HttpClient Client { get; set; }
        protected INetworkService NetworkService { get; set; }


        public BaseConsumer(INetworkService networkService)
        {
            NetworkService = networkService;
            Client = new HttpClient()
            {
                MaxResponseContentBufferSize = 256000,
                Timeout = TimeSpan.FromSeconds(30)
            };
        }

        //protected Uri GetWSURL() => Constants.BASE_URI;

        protected async Task<T> MakeGetCall<T>(string urlFormat)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    var response = await Client.GetAsync(urlFormat);
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        //Crashes.TrackError(new Exception(result));
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception)
                {
                    //Crashes.TrackError(ex);
                    throw new Exception(Messages.GeneralErrorMessage);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakePostCall<T>(string urlFormat, dynamic myObj, bool isOauth = false)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    string strContent = JsonConvert.SerializeObject(myObj);

                    var response = await Client.PostAsync(urlFormat, new StringContent(strContent, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var strResult = response.Content.ReadAsStringAsync().Result;
                        return (isOauth) ? (T)Convert.ChangeType(strResult, typeof(string)) : JsonConvert.DeserializeObject<T>(strResult);
                    }
                    else
                    {
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            throw new UnauthorizedAccessException();
                        }
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        //Crashes.TrackError(new Exception(result), StringExtension.ToDictionary(Messages.ErrorTitle, myObj));
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    //Crashes.TrackError(ex);
                    ex.ToString();
                    throw new Exception(Messages.GeneralErrorMessage);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakePutCall<T>(string urlFormat, dynamic myObj)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    string strContent = JsonConvert.SerializeObject(myObj);
                    var response = await Client.PutAsync(urlFormat, new StringContent(strContent, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        //Crashes.TrackError(new Exception(result), StringExtension.ToDictionary(Messages.ErrorTitle, myObj));
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception)
                {
                    //Crashes.TrackError(ex);
                    throw new Exception(Messages.GeneralErrorMessage);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakeDeleteCall<T>(string urlFormat)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    var response = await Client.DeleteAsync(urlFormat);
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        //Crashes.TrackError(new Exception(result));
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception)
                {
                    //Crashes.TrackError(ex);
                    throw new Exception(Messages.GeneralErrorMessage);
                }
            };
            return await NetworkService.RunFunction(f);
        }
    }
}
