﻿using NeuroWorkoutApp.Exceptions;
using NeuroWorkoutApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
namespace NeuroWorkoutApp.Services
{
   public class NetworkService : INetworkService, IDisposable
    {
        public event Action<bool> ConnectivityChanged;

        public NetworkService()
        {
            Connectivity.ConnectivityChanged += Current_ConnectivityChanged;
        }

        public bool IsConnected
        {
            get
            {
                var current = Connectivity.NetworkAccess;

                if (current == NetworkAccess.Internet)
                {
                    return true; // Connection to internet is available
                }
                return false;
            }
        }

        public void RunAction(Action action)
        {
            if (IsConnected)
                action?.Invoke();
            else
                throw new NotConnectedException();
        }

        public T RunFunction<T>(Func<T> func)
        {
            if (IsConnected)
                return func.Invoke();
            else
                throw new NotConnectedException("No se detecta una conexión a internet");
        }

        private void Current_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            ConnectivityChanged?.Invoke(IsConnected);
        }

        public void Dispose()
        {
            Connectivity.ConnectivityChanged -= Current_ConnectivityChanged;
        }
    }
}
