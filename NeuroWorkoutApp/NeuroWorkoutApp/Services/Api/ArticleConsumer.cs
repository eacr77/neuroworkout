﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.Services.Api
{
   public class ArticleConsumer : BaseConsumer
    {
        public ArticleConsumer(INetworkService networkService) : base(networkService)
        {

        }
        string direccionAPI = "api/Article/";
        public async Task<List<ArticleModel>> GetAllArticles()
        {
            try
            {
                return await MakeGetCall<List<ArticleModel>>(GetWSURL().AbsoluteUri + direccionAPI + "GetAllArticles");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
