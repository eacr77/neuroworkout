﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Utilities;
using NeuroWorkoutApp.Models.Base;

namespace NeuroWorkoutApp.Services.Api
{
   public class BaseConsumer
    {

        protected HttpClient Client { get; set; }
        protected INetworkService NetworkService { get; set; }
        public BaseConsumer(INetworkService networkService)
        {
            NetworkService = networkService;
            Client = new HttpClient()
            {
                MaxResponseContentBufferSize = Constants.MaxResponseContentBufferSize,
                Timeout = TimeSpan.FromSeconds(30)
            };
            //this.user = MainViewModel.GetInstance().User;
        }

        protected Uri GetWSURL() => Constants.BASE_URI;

        protected async Task<T> MakeGetCall<T>(string urlFormat)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    /* if (this.user != null && (!string.IsNullOrEmpty(this.user.Token) || !string.IsNullOrEmpty(this.user.IdCliente)))
                     {
                         string valuesAuthentication = string.Format("token:{0}:id:{1}", this.user.Token, this.user.IdCliente);
                         var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(valuesAuthentication);
                         string encodeAuthentication = System.Convert.ToBase64String(plainTextBytes);

                         Client.DefaultRequestHeaders.Authorization =
                         new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encodeAuthentication);
                     }*/
                    var response = await Client.GetAsync(urlFormat);
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Utilities.Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(Utilities.Messages.GeneralErrorMessage,ex);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakePostCall<T>(string urlFormat, dynamic myObj, bool isOauth = false)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    string strContent = JsonConvert.SerializeObject(myObj);
                    /*if (this.user != null && (!string.IsNullOrEmpty(this.user.Token) || !string.IsNullOrEmpty(this.user.IdCliente)))
                    {
                        string valuesAuthentication = string.Format("token:{0}:id:{1}", this.user.Token, this.user.IdCliente);
                        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(valuesAuthentication);
                        string encodeAuthentication = System.Convert.ToBase64String(plainTextBytes);

                        Client.DefaultRequestHeaders.Authorization =
                        new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", encodeAuthentication);
                    }*/
                    var response = await Client.PostAsync(urlFormat, new StringContent(strContent, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        var strResult = response.Content.ReadAsStringAsync().Result;
                        return (isOauth) ? (T)Convert.ChangeType(strResult, typeof(string)) : JsonConvert.DeserializeObject<T>(strResult);
                    }
                    else
                    {
                        if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            throw new UnauthorizedAccessException();
                        }
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Utilities.Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(Utilities.Messages.GeneralErrorMessage,ex);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakePutCall<T>(string urlFormat, dynamic myObj)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    string strContent = JsonConvert.SerializeObject(myObj);
                    var response = await Client.PutAsync(urlFormat, new StringContent(strContent, Encoding.UTF8, "application/json"));
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Utilities.Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(Utilities.Messages.GeneralErrorMessage,ex);
                }
            };
            return await NetworkService.RunFunction(f);
        }

        protected async Task<T> MakeDeleteCall<T>(string urlFormat)
        {
            Func<Task<T>> f = async () =>
            {
                try
                {
                    var response = await Client.DeleteAsync(urlFormat);
                    if (response.IsSuccessStatusCode || response.StatusCode == HttpStatusCode.BadRequest)
                    {
                        return JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    }
                    else
                    {
                        string result = response.Content.ReadAsStringAsync().Result;
                        var errorEntity = JsonConvert.DeserializeObject<BaseEntity>(result);
                        if (!string.IsNullOrEmpty(errorEntity.Message))
                            throw new Exception(errorEntity.Message);
                        else
                            throw new Exception(Utilities.Messages.GeneralErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(Utilities.Messages.GeneralErrorMessage,ex);
                }
            };
            return await NetworkService.RunFunction(f);
        }
    }
}

