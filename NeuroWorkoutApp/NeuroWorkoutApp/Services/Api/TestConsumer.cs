﻿using NeuroWorkoutApp.DTOs;
using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.Services.Api
{
   public class TestConsumer : BaseConsumer
    {
        public TestConsumer(INetworkService networkService) : base(networkService)
        {

        }
        string direccionAPI = "api/Test/";
        public async Task<int> SetEncuesta(TestModel model)
        {
            try
            {
                return await MakePostCall<int>(GetWSURL().AbsoluteUri + direccionAPI + "SetEncuesta", model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<PreguntaModel>> GetEncuesta(int opc)
        {
            try
            {
                return await MakePostCall<List<PreguntaModel>>(GetWSURL().AbsoluteUri + direccionAPI + "GetEncuesta",opc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
