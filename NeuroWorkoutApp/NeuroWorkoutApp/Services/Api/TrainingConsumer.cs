﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.Services.Api
{
   public class TrainingConsumer : BaseConsumer
    {

        public TrainingConsumer(INetworkService networkService) : base(networkService)
        {

        }
        string direccionAPI = "api/Training/";
        public async Task<EjercicioModel> GetEjercicioXId(Guid IdEjercicio)
        {
            try
            {
                return await MakePostCall<EjercicioModel>(GetWSURL().AbsoluteUri + direccionAPI + "GetEjercicioXId", IdEjercicio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<EjercicioWodModel>> GetEjercicioWod(Guid IdWod,int IdTipo)
        {
            try
            {
                object[] valores = { IdWod,IdTipo};
                return await MakePostCall<List<EjercicioWodModel>>(GetWSURL().AbsoluteUri + direccionAPI + "GetEjerciciosWod", valores);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<EjercicioModel>> GetAllEjercicios(int opc)
        {
            try
            {
                return await MakePostCall<List<EjercicioModel>>(GetWSURL().AbsoluteUri + direccionAPI + "GetAllEjercicios", opc);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<int> SetResultadoWod(WodResultModel model)
        {
            try
            {
                return await MakePostCall<int>(GetWSURL().AbsoluteUri + direccionAPI + "SetResultadoWod", model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<WodCompleto> GetWodDia(DateTime fecha)
        {
            try
            {
                return await MakePostCall<WodCompleto>(GetWSURL().AbsoluteUri + direccionAPI + "GetWodDia", fecha);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public async Task<WodCompleto> GetWodTest(int idTipoWod)
        {
            try
            {
                return await MakePostCall<WodCompleto>(GetWSURL().AbsoluteUri + direccionAPI + "GetWodTest", idTipoWod);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
