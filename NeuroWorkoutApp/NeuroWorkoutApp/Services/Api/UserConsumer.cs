﻿using NeuroWorkoutApp.Interfaces;
using NeuroWorkoutApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NeuroWorkoutApp.Services.Api
{
    public class UserConsumer: BaseConsumer
    {
        public UserConsumer(INetworkService networkService) : base(networkService)
        {

        }
        string direccionAPI = "api/User/";
        public async Task<User> Obtener(Guid IdUser)
        {
            try
            {
                return await MakePostCall<User>(GetWSURL().AbsoluteUri + direccionAPI + "GetUser", IdUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<User> Ingresar(User user)
        {
            try
            {
                return await MakePostCall<User>(GetWSURL().AbsoluteUri + direccionAPI + "Login", user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> Registrar(User user)
        {
            try
            {
                return await MakePostCall<int>(GetWSURL().AbsoluteUri + direccionAPI + "NewUser", user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> Actualizar(User user)
        {
            try
            {
                return await MakePostCall<int>(GetWSURL().AbsoluteUri + direccionAPI + "UpdateUser", user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<int> Eliminar(Guid IdUser)
        {
            try
            {
                return await MakePostCall<int>(GetWSURL().AbsoluteUri + direccionAPI + "DeleteUser", IdUser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Guid?> RecuperarUser(string Email)
        {
            try
            {
                return await MakePostCall<Guid?>(GetWSURL().AbsoluteUri + direccionAPI + "RecoveryUser", Email);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
