﻿using System;
using System.Collections.Generic;
using System.Text;
using NeuroWorkoutApp.Extensions;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Validations.Base;


namespace NeuroWorkoutApp.Validations
{
   public class LengthValidator : BaseValidator
    {
        public int MaxLength { get; set; }
        public int MinLength { get; set; }
        private int Length { get; set; }

        public LengthValidator()
        {
            Style = ErrorType.Error;
            MaxLength = 0; MinLength = 0; Length = 0;
            Message = "This field must be between {minlength} and {maxlength} characters. You entered {length} characters";
        }

        public override bool Check(object value)
        {
            bool result = false;
            BaseMessage = BaseMessage ?? Message;
            string texto = string.Empty;
            if (value.GetType() == typeof(string))
            {

                if (value != null)
                    Length = value.ToString().Length;
                if (Length >= MinLength && Length <= MaxLength)
                    result = true;
            }
            if (!result)
                Message = BaseMessage.Inject(new { minlength = MinLength.ToString(), maxlength = MaxLength.ToString(), length = Length.ToString() });

            return result;
        }
    }
}
