﻿using NeuroWorkoutApp.Extensions;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Validations.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class MinLengthValidator : BaseValidator
    {
        public int MinLength { get; set; }
        private int Length { get; set; }
        public MinLengthValidator()
        {
            Style = ErrorType.Error;
            Length = 0;
            MinLength = 0;
            Message = "This field must be great or equal than {maxlength} characters. You entered {length} characters";
        }
        public override bool Check(object value)
        {
            bool result = false;
            BaseMessage = BaseMessage ?? Message;
            string texto = string.Empty;
            if (value.GetType() == typeof(string))
            {
                if (value != null)
                    Length = value.ToString().Length;
                if (Length >= MinLength)
                    result = true;
            }
            if (!result)
                Message = BaseMessage.Inject(new { minlength = MinLength.ToString(), length = Length.ToString() });

            return result;
        }
    }
}
