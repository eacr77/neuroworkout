﻿using Newtonsoft.Json;
using System;

namespace NeuroWorkoutApp.Validations
{
   public class IsNotNullOrEmptyRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }

        public bool Check(T value)
        {
            if (value == null)
            {
                return false;
            }

            if (typeof(T) == typeof(string))
            {
                var str = value as string;
                return !string.IsNullOrWhiteSpace(str);
            }
            if (typeof(T) == typeof(bool))
            {
                return true;
            }

            else
            {
                var DefaultValue = Activator.CreateInstance<T>();
                string strContent1 = JsonConvert.SerializeObject(DefaultValue);
                string strContent2 = JsonConvert.SerializeObject(value);
                var result2 = (strContent1.Equals(strContent2));
                return !(result2);
            }

        }
    }
}
