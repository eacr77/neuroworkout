﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsEqualsThanRule<T> : IValidationRule<T>
    {
        public string ValidationMessage { get; set; }
        //public T CompareTo { get; set {  }; }
        private ValidatableObject<T> compareTo;
        public ValidatableObject<T> CompareTo
        {
            get { return compareTo; }
            set { compareTo = value; }
        }


        public bool Check(T value)
        {
            if (compareTo != null)
            {
                if (compareTo.Value != null && value != null)
                {
                    return value.Equals(compareTo.Value);
                }
            }
            return false;
        }
    }
}
