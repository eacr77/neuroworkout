﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsValidPasswordRule : IValidationRule<string>
    {
        public string ValidationMessage { get; set; }

        public bool Check(string value)
        {
            if (!string.IsNullOrEmpty(value))
                return CIDFares.Library.Validations.Validations.IsValidPassword(value);
            return false;
        }
    }
}
