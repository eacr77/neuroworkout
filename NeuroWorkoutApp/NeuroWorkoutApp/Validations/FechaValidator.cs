﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class FechaValidator : IValidationRule<DateTime>
    {
        //Esto es una prueba
        public string ValidationMessage { get; set; }
        public bool Check(DateTime value)
        {
            if (value.Year > 1930 && value.Year < 2005)
            {
                return true;
            }
            return false;
        }
    }
}
