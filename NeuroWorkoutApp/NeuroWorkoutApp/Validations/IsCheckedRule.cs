﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsCheckedRule : IValidationRule<bool>
    {
        public string ValidationMessage { get; set; }

        public bool Check(bool value)
        {
            return value;
        }
    }
}
