﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsNotSelectedRule : IValidationRule<int>
    {
        public string ValidationMessage { get; set; }

        public bool Check(int value)
        {
            if (value == 0)
            {
                return false;
            }
            return true;
        }
    }
}
