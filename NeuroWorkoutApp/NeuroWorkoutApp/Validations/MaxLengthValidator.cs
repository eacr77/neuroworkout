﻿using NeuroWorkoutApp.Extensions;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Validations.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class MaxLengthValidator : BaseValidator
    {
        public int MaxLength { get; set; }
        private int Length { get; set; }


        public MaxLengthValidator()
        {
            Style = ErrorType.Error;
            Length = 0;
            MaxLength = 0;
            Message = "This field must be less or equal than {maxlength} characters. You entered {length} characters";
        }

        public override bool Check(object value)
        {
            bool result = false;
            BaseMessage = BaseMessage ?? Message;
            string texto = string.Empty;
            if (value.GetType() == typeof(string))
            {
                if (value != null)
                    Length = value.ToString().Length;
                if (Length <= MaxLength)
                    result = true;
            }
            if (!result)
                Message = BaseMessage.Inject(new { maxlength = MaxLength.ToString(), length = Length.ToString() });

            return result;
        }
    }
}
