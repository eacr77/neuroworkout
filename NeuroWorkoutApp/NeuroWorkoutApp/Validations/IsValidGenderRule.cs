﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsValidGenderRule : IValidationRule<char>
    {
        public string ValidationMessage { get; set; }

        public bool Check(char value)
        {
            return (value.Equals('M') || value.Equals('F'));
        }
    }
}
