﻿using System;
using System.Collections.Generic;
using System.Text;
using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Validations.Base;


namespace NeuroWorkoutApp.Validations
{
   public class RequiredValidator : BaseValidator
    {
        public RequiredValidator()
        {
            Style = ErrorType.Error;
            Message = "This field is required.";
        }

        public override bool Check(object value)
        {
            if (value == null)
                return false;

            Type type = value.GetType();

            if (type == typeof(string))
                return IsValidString(value);
            if (type == typeof(int))
                return IsValidInt(value);
            if (type == typeof(double))
                return IsValidDouble(value);
            if (type == typeof(bool))
                return IsValidBool(value);
            if (type == typeof(DateTime))
                return IsValidDateTime(value);
            return false;
        }

        /// <summary>
        /// Se acepta como requerido cualquier valor string que no sea nulo, no esté vacío y no sean solo espacios en blanco
        /// </summary>
        /// <param name="value">valor a validar</param>
        /// <returns>Indica si es válido o no</returns>
        private bool IsValidString(object value)
        {
            return !string.IsNullOrWhiteSpace(value.ToString());
        }

        /// <summary>
        /// Se acepta como requerido cualquier valor que se pueda convertir a int
        /// </summary>
        /// <param name="value">valor a validar</param>
        /// <returns>Indica si es válido o no</returns>
        private bool IsValidInt(object value)
        {
            if (!int.TryParse(value.ToString(), out int FormatValue))
                return false;
            return true;
        }

        /// <summary>
        /// Se acepta como requerido cualquier valor que se pueda convertir a double
        /// </summary>
        /// <param name="value">valor a validar</param>
        /// <returns>Indica si es válido o no</returns>
        private bool IsValidDouble(object value)
        {
            if (!double.TryParse(value.ToString(), out double FormatValue))
                return false;
            return true;
        }

        /// <summary>
        /// Se acepta como requerido aquel cuyo valor sea true
        /// </summary>
        /// <param name="value">valor a validar</param>
        /// <returns>Indica si es válido o no</returns>
        private bool IsValidBool(object value)
        {
            if (!bool.TryParse(value.ToString(), out bool FormatValue))
                return false;
            return FormatValue;
        }

        /// <summary>
        /// Se acepta como requerido aquel cuyo valor no sea null
        /// </summary>
        /// <param name="value">valor a validar</param>
        /// <returns>Indica si es válido o no</returns>
        private bool IsValidDateTime(object value)
        {
            DateTime dateTime = DateTime.MinValue;
            try
            {
                dateTime = (DateTime)value;
            }
            catch (Exception) { }
            if (!dateTime.Equals(new DateTime(1900, 1, 1)))
                return true;
            return false;
        }
    }
}
