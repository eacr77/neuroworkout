﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Validations
{
   public class IsNotZeroRule : IValidationRule<decimal>
    {
        public string ValidationMessage { get; set; }

        public bool Check(decimal value)
        {
            if (value == 0)
            {
                return false;
            }
            return true;
        }
    }
}
