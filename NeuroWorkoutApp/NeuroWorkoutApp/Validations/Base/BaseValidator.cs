﻿using NeuroWorkoutApp.Helpers;
using NeuroWorkoutApp.Interfaces.Validation;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Validations.Base
{
   public class BaseValidator : BindableObject, IValidator
    {
        #region Bindables Properties
        public static readonly BindableProperty ParameterProperty = BindableProperty.Create(nameof(Parameter), typeof(object), typeof(BaseValidator), null, BindingMode.TwoWay);
        public object Parameter
        {
            get { return GetValue(ParameterProperty); }
            set { SetValue(ParameterProperty, value); }
        }
        #endregion

        public string Format { get; set; }

        public string Message { get; set; }
        protected string BaseMessage { get; set; }
        public ErrorType Style { get; set; }
        public PriorityType Priority { get; set; }

        public virtual bool Check(object value) => false;
    }
}
