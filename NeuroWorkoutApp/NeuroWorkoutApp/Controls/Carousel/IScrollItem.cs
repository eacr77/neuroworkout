﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Controls.Carousel
{
    public interface IConfigurableScrollItem
    {
        ScrollToConfiguration Config { get; set; }
    }
    interface IScrollItem : IConfigurableScrollItem
    {
    }
    public interface IGroupScrollItem : IConfigurableScrollItem
    {
        object GroupValue { get; set; }
    }
}
