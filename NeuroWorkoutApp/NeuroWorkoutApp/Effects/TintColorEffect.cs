﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Effects
{
   public class TintColorEffect : RoutingEffect
    {
        #region Bindables Properties
        //public Color TintColor { get; set; }
        public static readonly BindableProperty TintColorProperty = BindableProperty.CreateAttached("TintColor", typeof(Color), typeof(TintColorEffect), Color.DarkGray, propertyChanged: OnTintColorChanged);

        public static Color GetTintColor(BindableObject view)
        {
            return (Color)view.GetValue(TintColorProperty);
        }
        public static void SetTintColor(BindableObject view, bool value)
        {
            view.SetValue(TintColorProperty, value);
        }



        //#region OnPropertyChanged
        static void OnTintColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Xamarin.Forms.View;
            if (view == null)
            {
                return;
            }

            var toRemove = view.Effects.FirstOrDefault(e => e is TintColorEffect);
            if (toRemove == null)
            {
                view.Effects.Add(new TintColorEffect());
                //view.Effects.Remove(toRemove);
            }


            //bool hasPaddingEffect = (bool)newValue;
            //if (hasPaddingEffect)
            //{
            //    view.Effects.Add(new PaddingEffect());
            //}
            //else
            //{
            //    var toRemove = view.Effects.FirstOrDefault(e => e is PaddingEffect);
            //    if (toRemove != null)
            //    {
            //        view.Effects.Remove(toRemove);
            //    }
            //}
        }
        //#endregion



        #endregion

        public TintColorEffect() : base("com.eacr.neuroworkout")
        {
        }
    }
}
