﻿
using NeuroWorkoutApp.Infraestructure.Settings;
using Xamarin.Forms;

namespace NeuroWorkoutApp.Effects
{
    public class EntryLineColorEffect : RoutingEffect
    {
        public EntryLineColorEffect() : base(AppSettings.ResolutionGroupName +  ".EntryLineColorEffect")
        {
        }
    }
}
