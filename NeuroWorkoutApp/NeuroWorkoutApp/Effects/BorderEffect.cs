﻿using NeuroWorkoutApp.Infraestructure.Settings;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace NeuroWorkoutApp.Effects
{
    [Preserve(AllMembers = true)]
    public class BorderEffect : RoutingEffect
    {
        #region Bindables Properties
        public static readonly BindableProperty HasBorderEffectProperty = BindableProperty.CreateAttached("HasBorderEffect", typeof(bool), typeof(BorderEffect), false, propertyChanged: OnHasBorderEffectChanged);
        public static readonly BindableProperty BorderColorProperty = BindableProperty.CreateAttached("BorderColor", typeof(Color), typeof(BorderEffect), Color.Default);
        public static readonly BindableProperty BorderWidthProperty = BindableProperty.CreateAttached("BorderWidth", typeof(int), typeof(BorderEffect), 0);
        public static readonly BindableProperty BorderRadiusProperty = BindableProperty.CreateAttached("BorderRadius", typeof(int), typeof(BorderEffect), 0);
        #endregion

        #region Get/Set
        #region HasBorderEffectProperty
        public static bool GetHasBorderEffect(BindableObject view)
        {
            return (bool)view.GetValue(HasBorderEffectProperty);
        }
        public static void SetHasBorderEffect(BindableObject view, bool value)
        {
            view.SetValue(HasBorderEffectProperty, value);
        }
        #endregion

        #region BorderColorProperty
        public static Color GetBorderColor(BindableObject view)
        {
            return (Color)view.GetValue(BorderColorProperty);
        }
        public static void SetBorderColor(BindableObject view, Color value)
        {
            view.SetValue(BorderColorProperty, value);
        }
        #endregion

        #region BorderWidthProperty
        public static int GetBorderWidth(BindableObject view)
        {
            return (int)view.GetValue(BorderWidthProperty);
        }
        public static void SetBorderWidth(BindableObject view, int value)
        {
            view.SetValue(BorderWidthProperty, value);
        }
        #endregion

        #region BorderRadiusProperty
        public static int GetBorderRadius(BindableObject view)
        {
            return (int)view.GetValue(BorderRadiusProperty);
        }
        public static void SetBorderRadius(BindableObject view, int value)
        {
            view.SetValue(BorderRadiusProperty, value);
        }
        #endregion
        #endregion

        #region OnPropertyChanged
        static void OnHasBorderEffectChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Xamarin.Forms.View;
            if (view == null)
            {
                return;
            }

            bool hasBorderEffect = (bool)newValue;
            if (hasBorderEffect)
            {
                view.Effects.Add(new BorderEffect());
            }
            else
            {
                var toRemove = view.Effects.FirstOrDefault(e => e is BorderEffect);
                if (toRemove != null)
                {
                    view.Effects.Remove(toRemove);
                }
            }
        }
        #endregion

        #region Constructors
        public BorderEffect() : base(AppSettings.ResolutionGroupName + ".BorderEffect")
        {
        }
        #endregion
    }
}
