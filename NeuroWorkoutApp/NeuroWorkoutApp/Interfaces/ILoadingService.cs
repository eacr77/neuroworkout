﻿using Acr.UserDialogs;
using NeuroWorkoutApp.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Interfaces
{
   public interface ILoadingService
    {
        void Hide();
        void Show(string message = Messages.PleaseWait, MaskType mask = MaskType.Black);
    }
}
