﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using NeuroWorkoutApp.ViewModels.Base;

namespace NeuroWorkoutApp.Interfaces
{
   public interface INavigationService
    {
        Page CreateAndBindPage(Type viewModelType, object parameter);
        Task RootNavigation(Type viewModel, object parameter = null);
        Task CreateNavigation(Type viewModel, object parameter = null);
        Task RootNavigation<TViewModel>(object parameter = null) where TViewModel : BaseViewModel;
        Task CreateNavigation<TViewModel>(object parameter = null) where TViewModel : BaseViewModel;
        Task MasterPushAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel;
        Task MasterPushAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel;
        Task MasterPushAsync(Type viewModel, bool animated = true);
        Task MasterPushAsync(Type viewModel, object parameter = null, bool animated = true);
        Task PopAsync(bool animated = true);
        //Task PopModalAsync(bool animated = true);
        Task MasterPopToRootAsync(bool animated = true);

        Task PushAsync(Type viewModel, bool animated = true);
        Task PushAsync(Type viewModel, object parameter = null, bool animated = true);
        Task PushAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel;
        Task PushAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel;

        Task PushModalAsync(Page view, bool animated = true);

        //Task PushModalAsync(Type viewModel, bool animated = true);
        //Task PushModalAsync(Type viewModel, object parameter = null, bool animated = true);
        //Task PushModalAsync<TViewModel>(bool animated = true) where TViewModel : BaseViewModel;
        //Task PushModalAsync<TViewModel>(object parameter = null, bool animated = true) where TViewModel : BaseViewModel;

        Task MasterPopAsync(bool animated = true);

        Task RemoveLastFromBackStackAsync();

        Task RemoveBackStackAsync();
    }
}
