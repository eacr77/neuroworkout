﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Interfaces
{
   public interface INotificationManager
    {
        event EventHandler NotificationReceived;

        void Initialize();

        void SendNotification(string title, string message, string type);

        void ReceiveNotification(string title, string message, string type);
    }
}
