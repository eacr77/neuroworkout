﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Interfaces.Validation
{
   public interface IErrorStyle
    {
        void ShowError(Xamarin.Forms.View view, string message);
        void RemoveError(Xamarin.Forms.View view);
    }
}
