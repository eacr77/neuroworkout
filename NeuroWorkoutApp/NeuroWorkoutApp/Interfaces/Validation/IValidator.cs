﻿using NeuroWorkoutApp.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Interfaces.Validation
{
   public interface IValidator
    {
        object Parameter { get; set; }
        ErrorType Style { get; set; }
        string Message { get; set; }
        string Format { get; set; }
        PriorityType Priority { get; set; }
        bool Check(object value);
    }
}
