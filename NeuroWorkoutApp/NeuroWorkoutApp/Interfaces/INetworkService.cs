﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Interfaces
{
   public interface INetworkService
    {
        bool IsConnected { get; }
        void RunAction(Action action);
        T RunFunction<T>(Func<T> func);
        event Action<bool> ConnectivityChanged;
    }
}
