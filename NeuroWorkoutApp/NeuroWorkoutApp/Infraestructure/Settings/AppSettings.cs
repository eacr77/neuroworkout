﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuroWorkoutApp.Infraestructure.Settings
{
    public static class AppSettings
    {
        public static bool Test { get; set; }
        //Para pruebas
        public static Uri BASE_URI = new Uri("http://192.168.100.6:8090/");
        public const string ResolutionGroupName = "com.eacr.NeuroWorkoutapp";
    }
}
